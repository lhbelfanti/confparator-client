const CracoLessPlugin = require('craco-less');

// To modify one of the exposed variables:
// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

module.exports = {
	plugins: [
		{
			plugin: CracoLessPlugin,
			options: {
				lessLoaderOptions: {
					lessOptions: {
						modifyVars: {
							// Font
							"@font-family": "'Cookie', cursive",
							"@font-size-base": "40px",
							// Main colors
							"@text-color": "#000000",
							"@primary-color": "#b85b3f",
							"@error-color": "#e45a7d",
							"@success-color": "#3fb85b",
							"@disabled-color": "@primary-1",
							"@primary-1": "#f7eee9",
							"@primary-2": "#dec3b6",
							"@primary-3": "#dec3b6",
							"@primary-4": "#d19f8a",
							"@primary-5": "#c47c62",
							"@primary-6": "#b85b3f",
							"@primary-7": "#913f2a",
							"@primary-8": "#6b271a",
							"@primary-9": "#612500",
							"@primary-10": "#45150d",
							"@text-selection-bg": "@primary-3",
							// Layout and components general props
							"@layout-body-background": "@primary-3",
							"@body-background": "#f7eee9",
							"@component-background": "@body-background",
							"@border-width-base": "3px",
							"@border-color-base": "@primary-color",
							// List items / table cells background color (active and hover state)
							"@item-hover-bg": "@primary-5",
							//Input component
							"@input-placeholder-color": "fade(@text-color, 50%)",
							// Button component
							"@btn-disable-border": "fade(@text-color, 20%)",
							"@btn-disable-bg": "fade(@text-color, 15%)",
							"@btn-height-base": "75px",
							"@btn-height-sm": "70px",
							"@btn-font-size-sm": "20px",
							"@btn-height-lg": "100px",
							"@btn-font-size-lg": "60px",
							"@btn-padding-horizontal-base": "60px",
							"@btn-padding-horizontal-lg": "80px",
							// Select component
							"@select-item-selected-bg": "@primary-color",
							"@select-dropdown-font-size": "20px",
							// Switch component
							"@switch-height": "44px",
							"@switch-min-width": "88px",
							// Navigation Steps component
							"@steps-icon-font-size": "25px",
							"@steps-vertical-tail-width": "0px",
							// Form
							"@label-required-color": "@error-color"
						},
						javascriptEnabled: true,
					},
				},
			},
		},
	],
};