export const versionRegex = /[0-9]\d*(\.[0-9]\d*)*/;
export const branchRegex = /[a-zA-Z0-9-_]+/;
export const renameCreateInputRegex = /(^[a-zA-Z])+[a-zA-Z0-9.\-_+=*/"'(){}]*$/;

export const isValidComparatorURL = (comparison: string) => {
  const comparisonURLRegex = /[a-zA-Z0-9-_]+:[0-9]\d*(\.[0-9]\d*)*...[a-zA-Z0-9-_]+:[0-9]\d*(\.[0-9]\d*)*/;

  return comparison.match(comparisonURLRegex);
};

export const validateFullMatchRegex = (str: string, regex: RegExp) => {
  const matches = str.match(regex);

  return !!(matches && matches[0] === str);
};
