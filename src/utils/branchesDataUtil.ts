import { BranchesDataResponse, BranchObject } from "../responseTypes";

export const getBranches = (data: BranchesDataResponse) => {
  const branchesArray = [];
  const branchesData = data.branches;
  let branchObject: BranchObject;
  for (const index in branchesData) {
    if (branchesData.hasOwnProperty(index)) {
      branchObject = branchesData[index];
      for (const prop in branchObject) {
        if (branchObject.hasOwnProperty(prop)) {
          branchesArray.push(prop);
        }
      }
    }
  }

  return branchesArray;
};

export const getVersions = (data: BranchesDataResponse, branch: string | undefined) => {
  if (branch === undefined) {
    return [];
  }

  let versionsArray: string[] = [];
  const branchesData = data.branches;
  let branchObject: BranchObject;
  for (const index in branchesData) {
    if (branchesData.hasOwnProperty(index)) {
      branchObject = branchesData[index];
      for (const prop in branchObject) {
        if (branchObject.hasOwnProperty(prop)) {
          if (branch === prop) {
            versionsArray = branchObject[prop];
          }
        }
      }
    }
  }

  return versionsArray;
};
