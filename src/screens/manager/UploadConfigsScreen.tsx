import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { StepItem } from "../../components/AppSteps";
import Request from "../../components/Request";
import UploadSteps from "../../components/UploadSteps";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import useIsMounted from "../../hooks/useIsMounted";
import { BranchesDataResponse } from "../../responseTypes";

const UploadConfigsScreen = () => {
  const [data, setData] = useState<BranchesDataResponse>({ branches: [] });
  const [loading, setLoading] = useState(true);
  const isMounted = useIsMounted();

  const { app }: { app: string } = useParams();
  const url = `${API.list}/${app}`;

  const steps: StepItem[] = [
    { title: Texts.branch, description: Texts.selectBranch },
    { title: Texts.version, description: Texts.selectVersion },
    { title: Texts.configurations, description: Texts.selectConfigFiles }
  ];

  const onFetchSuccess = (res: BranchesDataResponse) => {
    if (isMounted()) {
      setLoading(false);
      setData(res);
    }
  };

  if (loading) {
    return (
      <Request<BranchesDataResponse>
        url={url}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  return <UploadSteps steps={steps} data={data} />;
};

export default UploadConfigsScreen;
