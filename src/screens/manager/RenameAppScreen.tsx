import React, { useState } from "react";
import { useParams } from "react-router-dom";
import BasicDataForm from "../../components/BasicDataForm";
import Request from "../../components/Request";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import useIsMounted from "../../hooks/useIsMounted";
import { AppObject, AppSelectionScreenResponse, BasicResponse } from "../../responseTypes";

const RenameAppScreen = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<AppObject>({ uri: "", name: "" });
  const [newData, setNewData] = useState<AppObject>({ name: "", uri: "" });
  const isMounted = useIsMounted();
  const url: string = API.list;
  const { app }: { app: string } = useParams();

  const onFetchSuccess = (res: AppSelectionScreenResponse) => {
    if (isMounted()) {
      res.apps.map((item, key) => {
        if (item.uri === app) {
          setData(item);
        }

        return item;
      });

      setLoading(false);
    }
  };

  if (loading) {
    return (
      <Request<AppSelectionScreenResponse>
        url={url}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  if (newData.name !== "" && newData.uri !== "") {
    return (
      <Request<BasicResponse>
        url={API.rename}
        method={"PUT"}
        body={newData}
        requestErrorText={Texts.renameError}
        requestSuccessText={Texts.renamedSuccessfully}
        redirectUrl={"/manage/"}
      />
    );
  }

  return (
    <BasicDataForm
      name={data.name}
      namePlaceholder={Texts.namePlaceholder}
      uri={data.uri}
      uriPlaceholder={Texts.uriPlaceholder}
      onValuesSet={(values) => setNewData(values)}
    />
  );
};

export default RenameAppScreen;
