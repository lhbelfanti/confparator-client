import { Col, Row } from "antd";
import React from "react";
import AppButton from "../../components/AppButton";
import * as Texts from "../../configs/texts.json";
import AppSelectionScreen from "../comparator/AppSelectionScreen";

const ManagerAppSelectionScreen = () => {
  return (
    <Row align={"middle"} justify={"center"}>
      <Col span={24}>
        <Row align={"middle"} justify={"center"}>
          <AppSelectionScreen />
        </Row>
        <Row align={"middle"} justify={"center"}>
          {Texts.or}
        </Row>
        <Row align={"middle"} justify={"center"}>
          <AppButton path={"create/"} text={Texts.createNewApp} size={"middle"} disabled={false} />
        </Row>
      </Col>
    </Row>
  );
};

export default ManagerAppSelectionScreen;
