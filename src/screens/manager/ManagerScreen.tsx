import { Col, Divider, Row } from "antd";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import AppButton from "../../components/AppButton";
import Request from "../../components/Request";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import useIsMounted from "../../hooks/useIsMounted";
import { AppValidationResponse } from "../../responseTypes";
import "./ManagerScreen.less";

const ManagerScreen = () => {
  const { app }: { app: string } = useParams();
  const [loading, setLoading] = useState(true);
  const [appName, setAppName] = useState<string | null>("");
  const isMounted = useIsMounted();

  const onFetchSuccess = (res: AppValidationResponse) => {
    if (isMounted()) {
      setLoading(res.name === null);
      setAppName(res.name);
    }
  };

  if (loading) {
    return (
      <Request<AppValidationResponse>
        url={API.validate}
        params={{ app }}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  return (
    <Col>
      <Row align={"middle"} justify={"center"} gutter={[20, 20]}>
        <Col xs={24}>
          <Row align={"middle"} justify={"center"}>
            {Texts.selectedApplication}
          </Row>
          <Row align={"middle"} justify={"center"}>
            <div className={"app-text"}>{appName}</div>
          </Row>
        </Col>
      </Row>
      <Divider plain>{Texts.manage}</Divider>
      <Row align={"middle"} justify={"center"} gutter={[8, 8]}>
        <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8}>
          <Row align={"middle"} justify={"center"}>
            <AppButton path={"rename/"} text={Texts.renameApp} size={"middle"} disabled={false} />
          </Row>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8}>
          <Row align={"middle"} justify={"center"}>
            <AppButton path={"upload/"} text={Texts.uploadConfigs} size={"middle"} disabled={false} />
          </Row>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8}>
          <Row align={"middle"} justify={"center"}>
            <AppButton path={"delete/"} text={Texts.deleteSection} size={"middle"} disabled={false} />
          </Row>
        </Col>
      </Row>
    </Col>
  );
};

export default ManagerScreen;
