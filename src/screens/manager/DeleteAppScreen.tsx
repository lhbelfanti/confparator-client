import React, { useState } from "react";
import { useParams } from "react-router-dom";
import DeleteElements from "../../components/DeleteElements";
import Request from "../../components/Request";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import useIsMounted from "../../hooks/useIsMounted";
import { BranchesDataResponse } from "../../responseTypes";

const DeleteAppScreen = () => {
  const [data, setData] = useState<BranchesDataResponse>({ branches: [] });
  const [loading, setLoading] = useState(true);
  const isMounted = useIsMounted();

  const { app }: { app: string } = useParams();
  const url = `${API.list}/${app}`;

  const onFetchSuccess = (res: BranchesDataResponse) => {
    if (isMounted()) {
      setLoading(false);
      setData(res);
    }
  };

  if (loading) {
    return (
      <Request<BranchesDataResponse>
        url={url}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  return <DeleteElements app={app} branchesData={data} />;
};

export default DeleteAppScreen;
