import React, { useState } from "react";
import BasicDataForm from "../../components/BasicDataForm";
import Request from "../../components/Request";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import { AppObject, BasicResponse } from "../../responseTypes";

const CreateNewAppScreen = () => {
  const [newData, setNewData] = useState<AppObject>({ name: "", uri: "" });

  if (newData.name !== "" && newData.uri !== "") {
    return (
      <Request<BasicResponse>
        url={API.create}
        method={"POST"}
        body={newData}
        requestErrorText={Texts.createError}
        requestSuccessText={Texts.createdSuccessfully}
        redirectUrl={"/manage/"}
      />
    );
  }

  return (
    <BasicDataForm
      namePlaceholder={Texts.namePlaceholder}
      uriPlaceholder={Texts.uriPlaceholder}
      onValuesSet={(values) => setNewData(values)}
    />
  );
};

export default CreateNewAppScreen;
