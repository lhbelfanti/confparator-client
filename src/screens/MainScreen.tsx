import { Row, Space } from "antd";
import AppButton from "../components/AppButton";
import * as Texts from "../configs/texts.json";

const MainScreen = () => {
  return (
    <Row justify={"center"}>
      <Space size={50}>
        <AppButton path={"compare/"} text={Texts.compare} size={"large"} />
        <AppButton path={"manage/"} text={Texts.manage} size={"large"} />
      </Space>
    </Row>
  );
};

export default MainScreen;
