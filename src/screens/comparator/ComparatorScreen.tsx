import { useCallback, useState } from "react";
import { useParams } from "react-router-dom";
import Comparator from "../../components/Comparator";
import ComparatorBar from "../../components/ComparatorBar";
import Request from "../../components/Request";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import useIsMounted from "../../hooks/useIsMounted";
import { BranchesDataResponse } from "../../responseTypes";
import { isValidComparatorURL } from "../../utils/regexParser";

const ComparatorScreen = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<BranchesDataResponse>({ branches: [] });
  const [comparatorUrl, setComparatorUrl] = useState("");
  const isMounted = useIsMounted();

  const { app, comparison }: { app: string; comparison: string } = useParams();
  const url = `${API.list}/${app}`;

  if (comparison && comparatorUrl === "" && isValidComparatorURL(comparison)) {
    setComparatorUrl(comparison);
  }

  const onFetchSuccess = (res: BranchesDataResponse) => {
    if (isMounted()) {
      setLoading(false);
      setData(res);
    }
  };

  const onCompareClicked = useCallback(
    (branchVersion1: string | undefined, branchVersion2: string | undefined) => {
      const newURL = `${branchVersion1}...${branchVersion2}`;
      if (comparatorUrl !== newURL) {
        setComparatorUrl(`${branchVersion1}...${branchVersion2}`);
      }
    },
    [comparatorUrl, setComparatorUrl]
  );

  if (loading) {
    return (
      <Request<BranchesDataResponse>
        url={url}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  if (data.branches.length > 0) {
    return (
      <>
        <ComparatorBar comparatorUrl={comparatorUrl} branchesData={data} onCompareClicked={onCompareClicked} />
        {comparatorUrl !== "" ? <Comparator url={comparatorUrl} /> : <></>}
      </>
    );
  }

  return <></>;
};

export default ComparatorScreen;
