import { Col, Row } from "antd";
import { useCallback, useState } from "react";
import AppButton from "../../components/AppButton";
import AppSelect from "../../components/AppSelect";
import Request from "../../components/Request";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import useIsMounted from "../../hooks/useIsMounted";
import { AppObject, AppSelectionScreenResponse } from "../../responseTypes";

const AppSelectionScreen = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<AppSelectionScreenResponse>({ apps: [] });
  const isMounted = useIsMounted();
  const [selectedApp, setSelectedApp] = useState<AppObject>({ uri: "", name: "" });

  const { apps } = data;
  const url: string = API.list;

  const onAppSelected = useCallback(
    (value: string | undefined) => {
      if (apps.length > 0) {
        for (const app of apps) {
          if (app && app.hasOwnProperty("name") && app.name === value) {
            setSelectedApp(app);
          }
        }
      }
    },
    [apps, setSelectedApp]
  );

  const onFetchSuccess = (res: AppSelectionScreenResponse) => {
    if (isMounted()) {
      setLoading(false);
      setData(res);
    }
  };

  if (loading) {
    return (
      <Request<AppSelectionScreenResponse>
        url={url}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  if (apps.length > 0) {
    const names: string[] | undefined = [];
    apps.map((item, key) => {
      if (names) {
        names.push(item.name);
      }

      return names;
    });

    return (
      <Row align={"middle"} justify={"center"} gutter={[8, 8]}>
        <Col span={24}>
          <Row align={"middle"} justify={"center"}>
            <AppSelect placeholderText={Texts.selectApp} data={names} onValueChange={onAppSelected} />
          </Row>
        </Col>
        <Col span={24}>
          <Row align={"middle"} justify={"center"}>
            <AppButton path={`${selectedApp.uri}/`} text={Texts.select} size={"middle"} disabled={!selectedApp.uri} />
          </Row>
        </Col>
      </Row>
    );
  }

  return <></>;
};

export default AppSelectionScreen;
