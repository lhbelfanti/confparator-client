/***
 * Comparator
 ***/
export type AppSelectionScreenResponse = {
  apps: AppObject[];
};

export type AppObject = {
  uri: string;
  name: string;
};

export type BranchesDataResponse = {
  branches: BranchObject[];
};

export type BranchObject = {
  [key: string]: string[];
};

export type ComparatorResponse = {
  configs: ConfigsObject[];
};

export type ConfigsObject = {
  [key: string]: {
    old: object;
    new: object;
  };
};

/***
 * Manager
 ***/

export type AppValidationResponse = {
  name: string | null;
};

export type BasicResponse = {
  status: string;
};
