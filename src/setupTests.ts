import "@testing-library/jest-dom";
import axios from "axios";
import "jest-canvas-mock";

// @ts-ignore
axios.defaults.adapter = require("axios/lib/adapters/http");

// Problem
// https://stackoverflow.com/questions/39830580/jest-test-fails-typeerror-window-matchmedia-is-not-a-function
// which led me here to this solution
// https://stackoverflow.com/questions/64813447/cannot-read-property-addlistener-of-undefined-react-testing-library
const f = () => {
  return {
    addListener: jest.fn(),
    removeListener: jest.fn()
  };
};
global.matchMedia = global.matchMedia || f;

// Problem
// https://github.com/ant-design/ant-design/issues/9412
// solution
// https://github.com/ant-design/ant-design/issues/9412#issuecomment-905496603

// List of message warnings we want to filter from warning logs in tests
const filteredWarnMessages: string[] = ["async-validator:"];
const filteredWarnVariables: string[] = [];

const privateWarnLog = console.warn;
jest.spyOn(console, "warn").mockImplementation((msg: string, ...args: unknown[]) => {
  filteredWarnMessages.some((message) => msg.includes(message)) ||
  filteredWarnVariables.some((variable) => variable === args[0])
    ? jest.fn()
    : privateWarnLog(msg, ...args);
});
