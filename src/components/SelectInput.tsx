import { Col, Input, Row, Tooltip } from "antd";
import React, { ChangeEvent, useEffect, useState } from "react";
import * as Texts from "../configs/texts.json";
import { validateFullMatchRegex } from "../utils/regexParser";
import AppButton from "./AppButton";
import AppSelect from "./AppSelect";
import "./SelectInput.less";
import { errorColor } from "./ComponentsStyle";

interface SelectInputProps {
  data: string[];
  selectTitle: string;
  selectPlaceholderText: string;
  inputTitle: string;
  inputPlaceholderText: string;
  buttonText: string;
  onButtonClicked: (value: string) => void;
  regex?: RegExp;
  tooltipMessage?: string;
}

const SelectInput = (props: SelectInputProps) => {
  const {
    data,
    selectTitle,
    selectPlaceholderText,
    inputTitle,
    inputPlaceholderText,
    buttonText,
    onButtonClicked,
    regex,
    tooltipMessage
  } = props;
  const [selectedValue, setSelectedValue] = useState<string | undefined>("");
  const [resetSelectValue, setResetSelectValue] = useState(false);
  const [inputValue, setInputValue] = useState<string | undefined>("");
  const [regexFormatError, setRegexFormatError] = useState(false);

  const onAppSelectValueChange = (value: string | undefined) => {
    setInputValue("");
    setSelectedValue(value);
  };

  const onInputValueChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setResetSelectValue(true);
    setInputValue(value);
    setSelectedValue(value);

    if (!regex) {
      return;
    }

    if (value === "") {
      setRegexFormatError(false);

      return;
    }

    setRegexFormatError(!validateFullMatchRegex(value, regex));
  };

  const onClick = () => {
    onButtonClicked(!!selectedValue ? selectedValue : "");
    setResetSelectValue(true);
    setInputValue("");
    setSelectedValue("");
  };

  useEffect(() => {
    if (resetSelectValue) {
      setResetSelectValue(false);
    }
  }, [resetSelectValue, setResetSelectValue]);

  return (
    <Col span={24}>
      <Row align={"middle"} justify={"center"} gutter={[8, 8]}>
        <Col span={10}>
          <Row align={"middle"} justify={"center"}>
            <div className={"element-title"}>{selectTitle}</div>
          </Row>
          <Row align={"middle"} justify={"center"}>
            <AppSelect
              data={data}
              placeholderText={selectPlaceholderText}
              onValueChange={onAppSelectValueChange}
              resetValue={resetSelectValue}
              disabled={data.length <= 0}
            />
          </Row>
        </Col>
        <Col span={4}>
          <Row align={"middle"} justify={"center"}>
            {Texts.or}
          </Row>
        </Col>
        <Col span={10}>
          <Row align={"middle"} justify={"center"}>
            <div className={"element-title"}>{inputTitle}</div>
          </Row>
          <Row align={"middle"} justify={"center"}>
            <Tooltip title={tooltipMessage} color={errorColor} visible={regexFormatError} placement={"bottom"}>
              <Input placeholder={inputPlaceholderText} value={inputValue} onChange={onInputValueChange} />
            </Tooltip>
          </Row>
        </Col>
      </Row>
      <Row align={"middle"} justify={"center"}>
        <AppButton size={"middle"} text={buttonText} onClick={onClick} disabled={!selectedValue || regexFormatError} />
      </Row>
    </Col>
  );
};

SelectInput.defaultProps = {
  tooltipMessage: ""
};

export default SelectInput;
