import { Col, Row } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import * as Texts from "../configs/texts.json";
import { BranchesDataResponse } from "../responseTypes";
import { getBranches, getVersions } from "../utils/branchesDataUtil";
import AppButton from "./AppButton";
import AppSelect from "./AppSelect";

interface BranchVersionProps {
  branchesData: BranchesDataResponse;
  defaultValue?: string;
  readyToCompare?: (branch: string, version: string) => void;
  reset?: () => void;
  // For the delete section
  useDeleteButtons: boolean;
  onBranchSelected?: (branch?: string) => void;
  onDeleteBranch?: () => void;
  onVersionSelected?: (version?: string) => void;
  onDeleteVersion?: () => void;
}

const BranchVersion = (props: BranchVersionProps) => {
  const {
    branchesData,
    defaultValue,
    readyToCompare,
    reset,
    useDeleteButtons,
    onBranchSelected,
    onDeleteBranch,
    onVersionSelected,
    onDeleteVersion
  } = props;
  const initialState: string[] = [];
  const [versions, setVersions] = useState<string[] | undefined>(initialState);
  const [selectedBranch, setSelectedBranch] = useState<string | undefined>("");
  const [selectedVersion, setSelectedVersion] = useState<string | undefined>("");
  let defaultBranch = "";
  let defaultVersion = "";

  const onBranchChange = useCallback(
    (value: string | undefined) => {
      setSelectedBranch(value);
      if (onBranchSelected) {
        onBranchSelected(value);
      }

      const versionsData = getVersions(branchesData, value);
      setVersions(versionsData);
    },
    [branchesData, setSelectedBranch, setVersions, onBranchSelected]
  );

  const onVersionChange = useCallback(
    (value: string | undefined) => {
      setSelectedVersion(value);
      if (onVersionSelected) {
        onVersionSelected(value);
      }
    },
    [setSelectedVersion, onVersionSelected]
  );

  useEffect(() => {
    if (selectedBranch && selectedVersion) {
      if (readyToCompare) {
        readyToCompare(selectedBranch, selectedVersion);
      }
    } else {
      if (reset) {
        reset();
      }
    }
  }, [selectedBranch, setSelectedVersion, selectedVersion, readyToCompare, reset]);

  if (defaultValue) {
    const split = defaultValue.split(":");
    defaultBranch = split[0];
    defaultVersion = split[1];
  }

  return (
    <Row align={"middle"} justify={"center"} gutter={[8, 8]}>
      <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
        <Row align={"middle"} justify={"center"}>
          <AppSelect
            placeholderText={Texts.selectBranch}
            data={getBranches(branchesData)}
            onValueChange={onBranchChange}
            defaultValue={defaultBranch}
          />
        </Row>
        {useDeleteButtons ? (
          <Row align={"middle"} justify={"center"}>
            <AppButton
              text={Texts.deleteBranch}
              size={"small"}
              disabled={!selectedBranch}
              confirmationPopup={true}
              popupTitle={Texts.deleteBranchWarningMessage}
              onConfirm={onDeleteBranch}
              style={{ marginTop: "15px" }}
            />
          </Row>
        ) : (
          <></>
        )}
      </Col>
      <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12}>
        <Row align={"middle"} justify={"center"}>
          <AppSelect
            placeholderText={Texts.selectVersion}
            data={versions}
            onValueChange={onVersionChange}
            defaultValue={defaultVersion}
          />
        </Row>
        {useDeleteButtons ? (
          <Row align={"middle"} justify={"center"}>
            <AppButton
              text={Texts.deleteVersion}
              size={"small"}
              disabled={!selectedVersion}
              confirmationPopup={true}
              popupTitle={Texts.deleteVersionWarningMessage}
              onConfirm={onDeleteVersion}
              style={{ marginTop: "15px" }}
            />
          </Row>
        ) : (
          <></>
        )}
      </Col>
    </Row>
  );
};

BranchVersion.defaultProps = {
  useDeleteButtons: false
};

export default BranchVersion;
