import { Steps } from "antd";
import React from "react";
import "./AppSteps.less";

const { Step } = Steps;

export type StepItem = { title: string; description: string };

interface AppStepsProps {
  items: StepItem[];
  currentStep: number;
  onStepChange: (step: number) => void;
}

const AppSteps = (props: AppStepsProps) => {
  const { items, currentStep, onStepChange } = props;

  const onChange = (index: number) => {
    if (index <= currentStep) {
      onStepChange(index);
    }
  };

  const steps = items.map((value, index) => {
    const last = items.length - 1 === index;

    return <Step key={index} title={value.title} description={value.description} className={last ? "last-step" : ""} />;
  });

  return (
    <Steps type={"navigation"} responsive={true} current={currentStep} onChange={onChange}>
      {steps}
    </Steps>
  );
};

export default AppSteps;
