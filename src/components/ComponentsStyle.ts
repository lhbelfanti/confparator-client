export const comparatorStyle = {
  variables: {
    light: {
      gutterBackground: "#d19f8a",
      gutterBackgroundDark: "#d19f8a",
      diffViewerTitleBackground: "#b85b3f",
      diffViewerTitleColor: "#fff",
      diffViewerTitleBorderColor: "#dec3b6"
    }
  },
  diffContainer: {
    margin: "20px 0",
    fontSize: 18
  }
};

export const errorColor = "#e45a7d";