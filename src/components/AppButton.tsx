import { QuestionCircleOutlined } from "@ant-design/icons";
import { Button, Popconfirm } from "antd";
import { SizeType } from "antd/lib/config-provider/SizeContext";
import { Link } from "react-router-dom";
import Texts from "../configs/texts.json";
import { errorColor } from "./ComponentsStyle";

interface AppButtonProps {
  text: string | undefined;
  size: SizeType;
  disabled: boolean;
  path?: string;
  onClick?: () => void;
  style?: object;
  confirmationPopup: boolean;
  popupTitle?: string;
  onConfirm?: () => void;
  onCancel?: () => void;
  forForm: boolean;
}

const AppButton = (props: AppButtonProps) => {
  const { size, text, disabled, path, confirmationPopup, popupTitle, onConfirm, onCancel, onClick, style, forForm } =
    props;

  const htmlType = forForm ? "submit" : "button";

  if (path !== undefined) {
    return (
      <Link to={path}>
        <Button
          type="primary"
          shape="round"
          size={size}
          disabled={disabled}
          onClick={onClick}
          style={style}
          htmlType={htmlType}
        >
          {text}
        </Button>
      </Link>
    );
  }

  if (confirmationPopup) {
    return (
      <Popconfirm
        title={popupTitle}
        onConfirm={onConfirm}
        onCancel={onCancel}
        okText={Texts.delete}
        cancelText={Texts.cancel}
        disabled={disabled}
        icon={<QuestionCircleOutlined style={{ color: errorColor }} />}
      >
        <Button type="primary" shape="round" size={size} disabled={disabled} style={style} htmlType={htmlType}>
          {text}
        </Button>
      </Popconfirm>
    );
  }

  return (
    <Button
      type="primary"
      shape="round"
      size={size}
      disabled={disabled}
      onClick={onClick}
      style={style}
      htmlType={htmlType}
    >
      {text}
    </Button>
  );
};

AppButton.defaultProps = {
  disabled: false,
  confirmationPopup: false,
  style: {},
  forForm: false
};

export default AppButton;
