import { Select } from "antd";
import { useCallback, useEffect, useState } from "react";
import "./AppSelect.less";

interface AppSelectProps {
  placeholderText: string;
  data: string[] | undefined;
  onValueChange: (value: string | undefined) => void;
  defaultValue?: string;
  resetValue?: boolean;
  disabled?: boolean;
}

const AppSelect = (props: AppSelectProps) => {
  const { placeholderText, data, onValueChange, defaultValue, resetValue, disabled } = props;
  const [selectedValue, setSelectedValue] = useState<string | undefined>(undefined);

  const isValidValue = useCallback(
    (value: string | undefined) => {
      if (data && data.length > 0 && value !== undefined) {
        return data.indexOf(value) > -1;
      }

      return false;
    },
    [data]
  );

  const options = useCallback(() => {
    const opts = [];
    if (data && data.length > 0) {
      for (let i = 0; i < data.length; i++) {
        opts.push({ key: i.toString(), value: data[i] });
      }
    }

    return opts;
  }, [data]);

  const handleChange = useCallback(
    (value: string | undefined) => {
      setSelectedValue(value);
      onValueChange(value);
    },
    [onValueChange, setSelectedValue]
  );

  // Handles value change
  useEffect(() => {
    if (defaultValue && isValidValue(defaultValue) && !selectedValue) {
      handleChange(defaultValue);
    }
  }, [data, defaultValue, handleChange, isValidValue, selectedValue]);

  // Handles when the select options are changed by an empty array or the selected value is not between the options.
  useEffect(() => {
    const noOptions = data && data.length === 0;
    const invalidValue = noOptions || !isValidValue(selectedValue);
    if (invalidValue && selectedValue !== undefined) {
      setSelectedValue(undefined);
      onValueChange(undefined);
    }
  }, [data, isValidValue, selectedValue, onValueChange]);

  // Handle the case where it is necessary to reset the dropdown
  useEffect(() => {
    if (resetValue) {
      setSelectedValue(undefined);
    }
  }, [resetValue]);

  return (
    <Select
      value={selectedValue}
      defaultValue={defaultValue ? defaultValue : placeholderText}
      className={"selector"}
      placeholder={placeholderText}
      showSearch
      options={options()}
      onChange={handleChange}
      disabled={disabled}
    />
  );
};

AppSelect.defaultProps = {
  resetValue: false,
  disabled: false
};

export default AppSelect;
