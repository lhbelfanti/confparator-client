import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import * as Texts from "../configs/texts.json";
import { BranchesDataResponse } from "../responseTypes";
import AppButton from "./AppButton";
import BranchVersion from "./BranchVersion";

interface ComparatorBarProps {
  comparatorUrl: string;
  branchesData: BranchesDataResponse;
  onCompareClicked: (branchVersion1: string, branchVersion2: string) => void;
}

const ComparatorBar = (props: ComparatorBarProps) => {
  const { branchesData, comparatorUrl } = props;
  const [branchVersion1, setBranchVersion1] = useState("");
  const [branchVersion2, setBranchVersion2] = useState("");
  const isDisabled = branchVersion1 === "" || branchVersion2 === "";
  const [autoCompare, setAutoCompare] = useState(!!comparatorUrl && isDisabled);
  let branchVersionDefault1 = "";
  let branchVersionDefault2 = "";

  if (comparatorUrl && isDisabled) {
    const splitUrl = comparatorUrl.split("...");
    branchVersionDefault1 = splitUrl[0];
    branchVersionDefault2 = splitUrl[1];
  }

  useEffect(() => {
    if (comparatorUrl && autoCompare && !isDisabled) {
      props.onCompareClicked(branchVersion1, branchVersion2);
      setAutoCompare(false);
    }
  }, [autoCompare, setAutoCompare, branchVersion1, branchVersion2, comparatorUrl, isDisabled, props]);

  return (
    <Row align={"middle"} justify={"center"} gutter={[8, 8]}>
      <BranchVersion
        branchesData={branchesData}
        defaultValue={branchVersionDefault1}
        readyToCompare={(branch: string, version: string) => {
          setBranchVersion1(`${branch}:${version}`);
        }}
        reset={() => {
          setBranchVersion1("");
        }}
      />
      <Col xs={24} sm={24} md={24} lg={24} xl={4} xxl={4}>
        <Row align={"middle"} justify={"center"}>
          <AppButton
            path={`${branchVersion1}...${branchVersion2}`}
            text={Texts.compare}
            disabled={isDisabled}
            size={"small"}
            onClick={() => {
              props.onCompareClicked(branchVersion1, branchVersion2);
            }}
          />
        </Row>
      </Col>
      <BranchVersion
        branchesData={branchesData}
        defaultValue={branchVersionDefault2}
        readyToCompare={(branch: string, version: string) => {
          setBranchVersion2(`${branch}:${version}`);
        }}
        reset={() => {
          setBranchVersion2("");
        }}
      />
    </Row>
  );
};

export default ComparatorBar;
