import { LoadingOutlined } from "@ant-design/icons";
import { Result, Row, Spin, Typography } from "antd";
import axios, { Method } from "axios";
import React, { useCallback, useEffect, useState } from "react";
import * as Texts from "../configs/texts.json";
import useIsMounted from "../hooks/useIsMounted";
import AppButton from "./AppButton";
const { Title } = Typography;

interface RequestProps<T = any> {
  url: string;
  method: Method;
  params?: object;
  body?: object;
  onSuccess?: (res: T) => void;
  requestSuccessText?: string;
  onFail?: (error: any) => void;
  requestErrorText?: string;
  onRetry?: () => void;
  redirectUrl?: string;
}

const Request = <T extends {}>(props: RequestProps<T>) => {
  const { url, method, params, body, onSuccess, requestSuccessText, onFail, requestErrorText, onRetry, redirectUrl } =
    props;

  const [loading, setLoading] = useState(true);
  const [requestFailed, setRequestFailed] = useState(false);
  const [requestSuccess, setRequestSuccess] = useState(false);
  const isMounted = useIsMounted();

  const onFetchFail = useCallback(
    (error: any) => {
      if (onFail) {
        onFail(error);
      }
    },
    [onFail]
  );

  const doRequest = useCallback(() => {
    axios
      .request({ url, method, params, data: body })
      .then((res) => {
        if (isMounted()) {
          setLoading(false);
        }

        if (method === "POST" || method === "PUT" || method === "DELETE") {
          setRequestSuccess(true);
        }

        if (onSuccess) {
          onSuccess(res.data);
        }
      })
      .catch((error) => {
        if (isMounted()) {
          setLoading(false);
          setRequestFailed(true);
          onFetchFail(error);
        }
      });
  }, [url, method, params, body, isMounted, setLoading, setRequestFailed, setRequestSuccess, onSuccess, onFetchFail]);

  useEffect(() => {
    doRequest();
  }, [doRequest]);

  const onRetryClicked = () => {
    setLoading(true);
    setRequestFailed(false);
    if (onRetry) {
      onRetry();
    }
    doRequest();
  };

  if (loading) {
    return (
      <Row align={"middle"} justify={"center"}>
        <Spin indicator={<LoadingOutlined style={{ fontSize: 100 }} spin />} size={"large"} />
      </Row>
    );
  }

  if (requestFailed) {
    return (
      <Result
        status="error"
        title={<Title level={5}>{requestErrorText}</Title>}
        extra={[<AppButton size={"small"} text={Texts.retry} onClick={onRetryClicked} key={"fail"} />]}
      />
    );
  }

  if (requestSuccess) {
    return (
      <Result
        status="success"
        title={<Title level={5}>{requestSuccessText}</Title>}
        extra={[<AppButton size={"small"} text={Texts.continue} path={redirectUrl} key={"success"} />]}
      />
    );
  }

  return null;
};

Request.defaultProps = {
  method: "GET",
  params: {},
  body: {},
  buttonText: ""
};

export default Request;
