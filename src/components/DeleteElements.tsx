import { Col, Divider, Row } from "antd";
import React, { useState } from "react";
import * as API from "../configs/api.json";
import * as Texts from "../configs/texts.json";
import { BasicResponse, BranchesDataResponse } from "../responseTypes";
import AppButton from "./AppButton";
import BranchVersion from "./BranchVersion";
import "./DeleteElements.less";
import Request from "./Request";

interface DeleteElementsProps {
  app: string;
  branchesData: BranchesDataResponse;
}

const DeleteElements = (props: DeleteElementsProps) => {
  const { app, branchesData } = props;
  const [selectedBranch, setSelectedBranch] = useState<string | undefined>("");
  const [selectedVersion, setSelectedVersion] = useState<string | undefined>("");
  const [params, setParams] = useState({});
  const deleteAppWarningMessage = Texts.deleteAppWarningMessage.replace("{app}", app);

  const onDeleteBranch = () => {
    if (selectedBranch) {
      setParams({ app, selectedBranch });
    }
  };

  const onDeleteVersion = () => {
    if (selectedVersion) {
      setParams({ app, selectedBranch, selectedVersion });
    }
  };

  const onDeleteApp = () => {
    setParams({ app });
  };

  if (Object.keys(params).length > 0) {
    return (
      <Request<BasicResponse>
        url={API.delete}
        method={"DELETE"}
        params={params}
        requestErrorText={Texts.deleteError}
        requestSuccessText={Texts.deletedSuccessfully}
        redirectUrl={`/manage/${app}/`}
      />
    );
  }

  return (
    <Col span={24}>
      <Row align={"middle"} justify={"center"}>
        {Texts.deleteSection}
      </Row>
      <Row align={"middle"} justify={"center"} className={"branch-version"}>
        <BranchVersion
          branchesData={branchesData}
          onBranchSelected={(branch: string | undefined) => {
            setSelectedBranch(branch);
          }}
          onVersionSelected={(version: string | undefined) => {
            setSelectedVersion(version);
          }}
          useDeleteButtons={true}
          onDeleteBranch={onDeleteBranch}
          onDeleteVersion={onDeleteVersion}
        />
      </Row>
      <div className={"danger-zone"}>
        <Row align={"middle"} justify={"center"}>
          <Divider plain>{Texts.dangerArea}</Divider>
        </Row>
        <Row align={"middle"} justify={"center"}>
          <AppButton
            text={Texts.deleteApp}
            size={"small"}
            confirmationPopup={true}
            popupTitle={deleteAppWarningMessage}
            onConfirm={onDeleteApp}
          />
        </Row>
        <Row align={"middle"} justify={"center"}>
          {Texts.deleteMessage}
        </Row>
      </div>
    </Col>
  );
};

export default DeleteElements;
