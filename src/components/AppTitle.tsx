import { Row } from "antd";
import * as Texts from "../configs/texts.json";
import "./AppTitle.less";

const AppTitle = () => {
  return (
    <Row justify="center">
      <span data-text={Texts.subtitle} className={"app-title-effect"}>
        {Texts.subtitle}
      </span>
    </Row>
  );
};

export default AppTitle;
