import { CheckCircleOutlined, CloseCircleOutlined, InboxOutlined } from "@ant-design/icons";
import { Col, message, Row, Upload } from "antd";
import { UploadChangeParam } from "antd/lib/upload/interface";
import React, { useState } from "react";
import * as Texts from "../configs/texts.json";
import useIsMounted from "../hooks/useIsMounted";
import AppButton from "./AppButton";
import "./MessagesComponent.less";
import "./UploadFiles.less";
const { Dragger } = Upload;

interface UploadFilesProps {
  uploadToUrl: string;
  buttonText: string;
  onButtonClicked: () => void;
}

const UploadFiles = (props: UploadFilesProps) => {
  const { uploadToUrl, buttonText, onButtonClicked } = props;
  const [disabled, setDisabled] = useState(true);
  const isMounted = useIsMounted();

  const onChange = (info: UploadChangeParam) => {
    const { status } = info.file;
    // TODO: change it after the server is coded
    if (status === "done") {
      message
        .success({
          content: `${info.file.name} ${Texts.messageUploadSuccess}`,
          className: "custom-success-message",
          icon: <CheckCircleOutlined />
        })
        .then();
    } else if (status === "error") {
      message
        .error({
          content: `${info.file.name} ${Texts.messageUploadFail}`,
          className: "custom-error-message",
          icon: <CloseCircleOutlined />
        })
        .then();
    }
  };

  return (
    <Col span={24}>
      <Row align={"middle"} justify={"center"}>
        <Dragger
          multiple={true}
          accept={".json"}
          onChange={(info: UploadChangeParam) => {
            if (info.file.status === "done" && isMounted()) {
              // For some reason this change of the state should be done here.
              // If it's done inside the onChange function the tests will fail.
              setDisabled(false);
            }
            onChange(info);
          }}
          action={uploadToUrl}
          data-testid={"uploader"}
        >
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">{Texts.dragDescription}</p>
          <p className="ant-upload-hint">{Texts.dragHint}</p>
        </Dragger>
      </Row>
      <Row align={"middle"} justify={"center"}>
        <AppButton
          size={"middle"}
          text={buttonText}
          onClick={onButtonClicked}
          disabled={disabled}
          style={{ marginTop: "10px" }}
        />
      </Row>
    </Col>
  );
};

export default UploadFiles;
