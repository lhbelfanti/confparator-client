import { Row } from "antd";
import React, { useState } from "react";
import * as API from "../configs/api.json";
import * as Texts from "../configs/texts.json";
import { BasicResponse, BranchesDataResponse } from "../responseTypes";
import { getBranches, getVersions } from "../utils/branchesDataUtil";
import { versionRegex } from "../utils/regexParser";
import AppSteps, { StepItem } from "./AppSteps";
import Request from "./Request";
import SelectInput from "./SelectInput";
import UploadFiles from "./UploadFiles";

interface UploadStepsProps {
  steps: StepItem[];
  data: BranchesDataResponse;
}

const UploadSteps = (props: UploadStepsProps) => {
  const { steps, data } = props;
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [branch, setBranch] = useState<string>("");
  const [version, setVersion] = useState<string>("");
  const [complete, setComplete] = useState(false);

  const onStepChange = (index: number) => {
    setCurrentStep(index);
  };

  const onBranchSelected = (value: string) => {
    if (value !== branch) {
      setVersion("");
    }
    setBranch(value);
    onStepChange(currentStep + 1);
  };

  const onVersionSelected = (value: string) => {
    setVersion(value);
    onStepChange(currentStep + 1);
  };

  const onFilesUploaded = () => {
    onStepChange(currentStep + 1);
    setComplete(true);
  };

  const stepComponent = () => {
    switch (currentStep) {
      case 0: // Branch
        return (
          <SelectInput
            data={getBranches(data)}
            selectTitle={Texts.existingBranches}
            selectPlaceholderText={Texts.selectBranch}
            inputTitle={Texts.createBranch}
            inputPlaceholderText={Texts.branch}
            buttonText={Texts.continue}
            onButtonClicked={onBranchSelected}
          />
        );
      case 1: // Version
        return (
          <SelectInput
            data={getVersions(data, branch)}
            selectTitle={Texts.existingVersions}
            selectPlaceholderText={Texts.selectVersion}
            inputTitle={Texts.createVersion}
            inputPlaceholderText={Texts.version}
            buttonText={Texts.continue}
            onButtonClicked={onVersionSelected}
            regex={versionRegex}
            tooltipMessage={Texts.versionFormatMessage}
          />
        );
      case 2: // Upload configs -- TODO: Change it after the server is coded
        return <UploadFiles uploadToUrl={"/test"} buttonText={Texts.finish} onButtonClicked={onFilesUploaded} />;
    }
  };

  if (complete) {
    return (
      <Request<BasicResponse>
        url={API.upload}
        method={"POST"}
        body={{ branch, version }}
        requestErrorText={Texts.uploadError}
        requestSuccessText={Texts.uploadedSuccessfully}
        redirectUrl={"/manage/"}
      />
    );
  }

  return (
    <Row align={"middle"} justify={"center"} gutter={[20, 20]}>
      {Texts.uploadConfigs}
      <AppSteps items={steps} currentStep={currentStep} onStepChange={onStepChange} />
      {stepComponent()}
    </Row>
  );
};

export default UploadSteps;
