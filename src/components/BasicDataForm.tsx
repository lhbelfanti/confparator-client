import { CloseCircleOutlined } from "@ant-design/icons";
import { Form, Input, message, Row, Tooltip } from "antd";
import React from "react";
import * as Texts from "../configs/texts.json";
import { AppObject } from "../responseTypes";
import { renameCreateInputRegex } from "../utils/regexParser";
import AppButton from "./AppButton";
import "./BasicDataForm.less";
import "./MessagesComponent.less";
import { errorColor } from "./ComponentsStyle";

interface BasicDataFormProps {
  name?: string;
  namePlaceholder?: string;
  uri?: string;
  uriPlaceholder?: string;
  onValuesSet: (data: AppObject) => void;
}

const BasicDataForm = (props: BasicDataFormProps) => {
  const { name, namePlaceholder, uri, uriPlaceholder, onValuesSet } = props;
  const initialValues = name && uri ? { name, uri } : {};
  const regexRule = {
    pattern: renameCreateInputRegex,
    message: (
      <Tooltip visible={true} color={errorColor} title={Texts.inputsTooltipMsg} placement={"bottom"} arrowPointAtCenter>
        <div className={"dummy-div"} />
      </Tooltip>
    )
  };

  const onFinish = (values: { name: string; uri: string }) => {
    onValuesSet(values);
  };

  const onFinishFailed = () => {
    message
      .error({
        content: Texts.formError,
        className: "custom-error-message",
        icon: <CloseCircleOutlined />
      })
      .then();
  };

  return (
    <Row align={"middle"} justify={"center"}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={initialValues}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label={Texts.name}
          name="name"
          rules={[{ required: true, message: Texts.inputAppNameWarning }]}
          className={"wrong-format"}
        >
          <Input placeholder={namePlaceholder} />
        </Form.Item>
        <Form.Item
          label={Texts.uri}
          name="uri"
          rules={[{ required: true, message: Texts.inputAppURIWarning }, regexRule]}
          className={"wrong-format"}
        >
          <Input placeholder={uriPlaceholder} />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <AppButton text={Texts.submit} size={"middle"} forForm={true} />
        </Form.Item>
      </Form>
    </Row>
  );
};

export default BasicDataForm;
