import { Row, Switch } from "antd";
import { useEffect, useState } from "react";
import ReactDiffViewer from "react-diff-viewer";
import * as API from "../configs/api.json";
import * as Texts from "../configs/texts.json";
import useIsMounted from "../hooks/useIsMounted";
import { ComparatorResponse } from "../responseTypes";
import "./Comparator.less";
import { comparatorStyle } from "./ComponentsStyle";
import Request from "./Request";

interface ComparatorProps {
  url: string;
}

const comparatorInitialValue: ComparatorResponse = { configs: [] };

const Comparator = (props: ComparatorProps) => {
  const { url } = props;
  const [currentURL, setCurrentURL] = useState("");
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(comparatorInitialValue);
  const [splitView, setSplitView] = useState(true);
  const isMounted = useIsMounted();

  useEffect(() => {
    if (url !== currentURL) {
      setLoading(true);
      setCurrentURL(url);
    }
  }, [currentURL, setCurrentURL, url, loading, setLoading]);

  const onFetchSuccess = (res: ComparatorResponse) => {
    if (isMounted()) {
      setLoading(false);
      setData(res);
    }
  };

  if (loading) {
    return (
      <Request<ComparatorResponse>
        url={`${API.compare}/${url}`}
        method={"GET"}
        onSuccess={onFetchSuccess}
        onRetry={() => {
          setLoading(true);
        }}
        requestErrorText={Texts.fetchError}
      />
    );
  }

  const diffElements = data.configs.map((item, key) => {
    let title,
      oldCfg,
      newCfg: string = "";
    for (title in item) {
      if (item.hasOwnProperty(title)) {
        oldCfg = JSON.stringify(item[title].old, null, 4);
        newCfg = JSON.stringify(item[title].new, null, 4);
      }
    }

    return (
      <ReactDiffViewer
        key={key}
        styles={comparatorStyle}
        oldValue={oldCfg}
        newValue={newCfg}
        leftTitle={title}
        splitView={splitView}
      />
    );
  });

  return (
    <Row align={"middle"} justify={"center"} className={"view-switch-row"}>
      <Switch
        className={"view-switch " + (!splitView ? "view-switch-color" : "")}
        checkedChildren={Texts.splitView}
        unCheckedChildren={Texts.normalView}
        size={"default"}
        defaultChecked
        onClick={() => setSplitView(!splitView)}
      />
      {diffElements}
    </Row>
  );
};

export default Comparator;
