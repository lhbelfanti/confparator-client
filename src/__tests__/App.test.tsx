import { render, screen } from "@testing-library/react";
import React from "react";
import App from "../App";

describe("App Component Tests Suite", () => {
  test("Renders correctly", () => {
    render(<App />);

    expect(screen.getByText(/Configs Comparator/i)).toBeInTheDocument();
  });
});
