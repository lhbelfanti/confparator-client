import { act, cleanup, renderHook } from "@testing-library/react-hooks";
import useIsMounted from "../../hooks/useIsMounted";

describe("useIsMounted Hook Tests Suite", () => {
  afterEach(cleanup);

  test("Returns true if the hook is mounted", () => {
    const { result } = renderHook(() => useIsMounted());
    expect(result.current()).toBe(true);
  });

  test("Returns false if the hook is unmounted", () => {
    const { result, unmount } = renderHook(() => useIsMounted());
    expect(result.current()).toBe(true);
    act(() => {
      unmount();
    });
    expect(result.current()).toBe(false);
  });

  test("Should be defined", () => {
    expect(useIsMounted).toBeDefined();
  });
});
