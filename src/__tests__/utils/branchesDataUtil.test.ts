import { BranchesDataResponse } from "../../responseTypes";
import { getBranches, getVersions } from "../../utils/branchesDataUtil";

describe("Branches Data Util Tests Suite", () => {
  const branchesData: BranchesDataResponse = {
    branches: [
      { a: ["1.0.0", "2.3.0", "3.0.0"] },
      { b: [] },
      { c: ["1.0.0", "1.3.2"] },
      { d: ["1.0.0"] },
      { e: ["1.0.0", "2.0.0", "3.0.0", "4.0.0", "5.0.0", "6.0.0"] }
    ]
  };

  test("getBranches function", () => {
    const result = ["a", "b", "c", "d", "e"];

    const branchesArray = getBranches(branchesData);
    expect(branchesArray).toStrictEqual(result);
  });

  test("getVersions function", () => {
    const result = ["1.0.0", "2.3.0", "3.0.0"];

    let versionsArray = getVersions(branchesData, "a");
    expect(versionsArray).toStrictEqual(result);

    versionsArray = getVersions(branchesData, undefined);
    expect(versionsArray).toStrictEqual([]);
  });
});
