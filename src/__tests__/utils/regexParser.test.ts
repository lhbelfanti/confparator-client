import { branchRegex, isValidComparatorURL, validateFullMatchRegex, versionRegex } from "../../utils/regexParser";

describe("Regex Parser Util Tests Suite", () => {
  test("Valid options for isValidComparatorURL", () => {
    const options = [
      "branchA:1.0.0...branchB:2.0.0",
      "branch1:1.0.0...branch1:1.0.0",
      "b:1...b:2",
      "b:1.0.0.1.0.3...b:2.2.2"
    ];

    for (const opt of options) {
      const result = isValidComparatorURL(opt);
      expect(result && result.length > 0).toBe(true);
    }
  });

  test("Invalid options for isValidComparatorURL", () => {
    const options = ["branchA:...branchB:2.0.0", "branch1:1.0.0...branch1:", "b...b", "...", ""];

    for (const opt of options) {
      const result = isValidComparatorURL(opt);
      expect(result).toBe(null);
    }
  });

  test("Valid options for validateFullMatchRegex", () => {
    const optionsBranches = ["testing", "2test", "TEST2", "2222", "my_branch", "my-branch"];

    for (const opt of optionsBranches) {
      const result = validateFullMatchRegex(opt, branchRegex);
      expect(result).toBe(true);
    }

    const optionsVersions = ["1", "1.0", "1.0.0.2", "11.2233.323.4455123"];

    for (const opt of optionsVersions) {
      const result = validateFullMatchRegex(opt, versionRegex);
      expect(result).toBe(true);
    }
  });

  test("Invalid options for validateFullMatchRegex", () => {
    const optionsBranches = [".dq", "", "+-/*^&|", "!@#$%*(){}?"];

    for (const opt of optionsBranches) {
      const result = validateFullMatchRegex(opt, branchRegex);
      expect(result).toBe(false);
    }

    const optionsVersions = ["", "a", "1.s0.0.2d", "11.233-3"];

    for (const opt of optionsVersions) {
      const result = validateFullMatchRegex(opt, versionRegex);
      expect(result).toBe(false);
    }
  });
});
