import { act, fireEvent, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import selectEvent from "react-select-event";
import { StepItem } from "../../components/AppSteps";
import UploadSteps from "../../components/UploadSteps";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import { BranchesDataResponse } from "../../responseTypes";

describe("UploadSteps Component Tests Suite", () => {
  const stepsMock: StepItem[] = [
    { title: Texts.branch, description: Texts.selectBranch },
    { title: Texts.version, description: Texts.selectVersion },
    { title: Texts.configurations, description: Texts.selectConfigFiles }
  ];

  const dataMock: BranchesDataResponse = {
    branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["3.0.0"] }]
  };

  const server = setupServer(
    rest.post("/test", (req, res, ctx) => {
      return res(ctx.json({ response: "200 OK" }));
    }),
    rest.post(API.upload, (req, res, ctx) => {
      return res(ctx.json({ response: "200 OK" }));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  let currentPath: Location<unknown>;
  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={[""]}>
        <Route
          path={"*"}
          render={(props) => {
            currentPath = props.location;

            return <UploadSteps steps={stepsMock} data={dataMock} />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getAllByRole("button")).toHaveLength(4);
    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toBeInTheDocument();
    for (const stepItem of stepsMock) {
      expect(screen.getByText(stepItem.title)).toBeInTheDocument();
      if (stepItem.description === stepsMock[0].description) {
        expect(screen.getAllByText(stepItem.description)).toHaveLength(2);
      } else {
        expect(screen.getByText(stepItem.description)).toBeInTheDocument();
      }
    }
  });

  test("Main functionality", async () => {
    renderComponent();

    // Step 1
    expect(screen.getAllByRole("button")).toHaveLength(4);
    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toBeInTheDocument();

    let comboBox = screen.getByRole("combobox");
    userEvent.click(comboBox);
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);
    let continueButton = screen.getByText(Texts.continue);
    userEvent.click(continueButton);

    // Step 2
    expect(screen.getAllByRole("button")).toHaveLength(4);
    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toBeInTheDocument();

    comboBox = screen.getByRole("combobox");
    userEvent.click(comboBox);
    await selectEvent.select(screen.getByRole("listbox"), ["1.0.0"]);
    continueButton = screen.getByText(Texts.continue);
    userEvent.click(continueButton);

    // Step 3
    const file = new File(["{test: 1}"], "test.json", { type: "application/json" });
    const input: HTMLInputElement = screen.getByTestId("uploader") as HTMLInputElement;

    await act(async () => {
      const flushPromises = () => new Promise(setImmediate);
      fireEvent.change(input, { target: { files: [file] } });
      await flushPromises();
    });

    expect(input.files).toHaveLength(1);
    await waitFor(() => {
      expect(screen.getByText(`test.json ${Texts.messageUploadSuccess}`)).toBeInTheDocument();
    });

    const finishButton = screen.getByText(Texts.finish);
    expect(finishButton).toBeEnabled();
    act(() => {
      userEvent.click(finishButton);
    });

    // Upload success
    await waitFor(() => {
      expect(screen.getByText(Texts.uploadedSuccessfully)).toBeInTheDocument();
    });

    const uploadSuccessButton = screen.getByText(Texts.continue);
    expect(uploadSuccessButton).toBeInTheDocument();
    userEvent.click(uploadSuccessButton);
    expect(currentPath.pathname).toBe("/manage/");
  });
});
