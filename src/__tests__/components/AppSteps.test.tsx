import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import AppSteps, { StepItem } from "../../components/AppSteps";
import * as Texts from "../../configs/texts.json";

describe("AppSteps Component Tests Suite", () => {
  const onStepChangeMock = jest.fn();
  const stepsMock: StepItem[] = [
    { title: Texts.branch, description: Texts.selectBranch },
    { title: Texts.version, description: Texts.selectVersion },
    { title: Texts.configurations, description: Texts.selectConfigFiles }
  ];

  function renderComponent(currentStep: number) {
    return render(<AppSteps items={stepsMock} currentStep={currentStep} onStepChange={onStepChangeMock} />);
  }

  test("Renders correctly", () => {
    renderComponent(2);

    expect(screen.getByText(Texts.branch)).toBeInTheDocument();
    expect(screen.getByText(Texts.selectBranch)).toBeInTheDocument();
    expect(screen.getByText(Texts.version)).toBeInTheDocument();
    expect(screen.getByText(Texts.selectVersion)).toBeInTheDocument();
    expect(screen.getByText(Texts.configurations)).toBeInTheDocument();
    expect(screen.getByText(Texts.selectConfigFiles)).toBeInTheDocument();
    expect(screen.getAllByRole("button")).toHaveLength(stepsMock.length);
  });

  test("On previous step clicked", () => {
    renderComponent(2);

    const buttons = screen.getAllByRole("button");
    userEvent.click(buttons[1]);
    expect(onStepChangeMock).toBeCalledTimes(1);
    expect(onStepChangeMock).toBeCalledWith(1);
    userEvent.click(buttons[2]);
    expect(onStepChangeMock).toBeCalledTimes(1);
  });

  test("On next step clicked", () => {
    renderComponent(0);

    userEvent.click(screen.getAllByRole("button")[1]);
    expect(onStepChangeMock).not.toBeCalled();
  });
});
