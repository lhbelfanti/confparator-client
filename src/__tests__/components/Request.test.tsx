import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Method } from "axios";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import Request from "../../components/Request";
import * as Texts from "../../configs/texts.json";

describe("Request Component Tests Suite", () => {
  type MockResponse = { apps: { uri: string; name: string }[] };

  const mockJson = {
    apps: [
      { uri: "app1", name: "Test 1" },
      { uri: "app2", name: "Test 2" },
      { uri: "app3", name: "Test 3" }
    ]
  };

  const server = setupServer(
    rest.get("/api/mock", (req, res, ctx) => {
      return res(ctx.json(mockJson));
    }),
    rest.post("/api/mock", (req, res, ctx) => {
      return res(ctx.json(mockJson));
    })
  );

  const fetchSuccessPromise = Promise.resolve();
  const onFetchSuccessMock = jest.fn(() => fetchSuccessPromise);
  const fetchFailPromise = Promise.resolve();
  const onFetchFail = jest.fn(() => fetchFailPromise);
  const retryPromise = Promise.resolve();
  const onRetryMock = jest.fn(() => retryPromise);

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  function renderComponent(method: Method, onRetry?: () => void, onFetchSuccess?: () => void) {
    return render(
      <Request<MockResponse>
        url={"/api/mock"}
        method={method}
        onSuccess={onFetchSuccess}
        onFail={onFetchFail}
        onRetry={onRetry}
      />
    );
  }

  test("GET request success", async () => {
    renderComponent("GET", onRetryMock, onFetchSuccessMock);

    await waitFor(() => {
      expect(onFetchSuccessMock).toBeCalledTimes(1);
      expect(screen.queryByRole("button")).not.toBeInTheDocument(); // Retry button
      expect(screen.queryByRole("img")).not.toBeInTheDocument(); // Loading spinner
    });

    await act(() => fetchSuccessPromise);
  });

  test("GET request failed", async () => {
    server.use(
      rest.get("/api/mock", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent("GET", onRetryMock);

    await waitFor(() => {
      expect(onFetchFail).toBeCalledTimes(1);
      expect(screen.getByRole("button")).toBeInTheDocument(); // Retry button
    });

    await act(() => fetchFailPromise);
  });

  test("GET request retry", async () => {
    server.use(
      rest.get("/api/mock", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent("GET", onRetryMock);

    let button: TargetElement;
    await waitFor(() => {
      expect(onFetchFail).toBeCalledTimes(1);
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // Retry button
    });

    await act(() => fetchFailPromise);

    act(() => {
      userEvent.click(button);
    });

    await act(() => retryPromise);

    await waitFor(() => {
      expect(onRetryMock).toBeCalledTimes(1);
      expect(onFetchFail).toBeCalledTimes(2);
      expect(screen.getByRole("button")).toBeInTheDocument(); // Retry button
    });

    await act(() => fetchFailPromise);
  });

  test("POST request success", async () => {
    renderComponent("POST");

    await waitFor(() => {
      expect(screen.getByText(Texts.continue)).toBeInTheDocument(); // Continue button
      expect(screen.getByRole("img")).toBeInTheDocument(); // Success tick image
    });
  });

  test("POST request failed", async () => {
    server.use(
      rest.post("/api/mock", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent("POST");

    await waitFor(() => {
      expect(onFetchFail).toBeCalledTimes(1);
      expect(screen.getByRole("button")).toBeInTheDocument(); // Retry button
    });

    await act(() => fetchFailPromise);
  });

  test("POST request retry", async () => {
    server.use(
      rest.post("/api/mock", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent("POST");

    let button: TargetElement;
    await waitFor(() => {
      expect(onFetchFail).toBeCalledTimes(1);
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // Retry button
    });

    await act(() => fetchFailPromise);

    act(() => {
      userEvent.click(button);
    });

    await act(() => retryPromise);

    await waitFor(() => {
      expect(onFetchFail).toBeCalledTimes(2);
      expect(screen.getByRole("button")).toBeInTheDocument(); // Retry button
    });

    await act(() => fetchFailPromise);
  });
});
