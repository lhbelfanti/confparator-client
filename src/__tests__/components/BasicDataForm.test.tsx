import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import BasicDataForm from "../../components/BasicDataForm";
import * as Texts from "../../configs/texts.json";

describe("BasicDataForm Component Tests Suite", () => {
  const onValuesSetMock = jest.fn();

  function renderComponent(name?: string, uri?: string) {
    return render(
      <BasicDataForm
        name={name}
        namePlaceholder={"Placeholder1"}
        uri={uri}
        uriPlaceholder={"Placeholder2"}
        onValuesSet={onValuesSetMock}
      />
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getAllByRole("textbox")).toHaveLength(2);
    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Placeholder1")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("Placeholder2")).toBeInTheDocument();
    expect(screen.queryByDisplayValue("Test1")).not.toBeInTheDocument();
    expect(screen.queryByDisplayValue("Test2")).not.toBeInTheDocument();
  });

  test("Renders correctly with default values", () => {
    renderComponent("Test1", "Test2");

    expect(screen.getAllByRole("textbox")).toHaveLength(2);
    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test1")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Test2")).toBeInTheDocument();
  });

  test("Uri input regex validation", async () => {
    renderComponent();

    expect(screen.queryByRole("tooltip")).not.toBeInTheDocument();

    const uriInput = screen.getByPlaceholderText("Placeholder2");
    userEvent.type(uriInput, ".");
    await waitFor(() => {
      expect(screen.getByRole("tooltip")).toBeInTheDocument();
    });

    userEvent.clear(uriInput);
    userEvent.type(uriInput, "Test");
    await waitFor(() => {
      expect(screen.queryByRole("tooltip")).not.toBeInTheDocument();
    });
  });

  test("Required field warning", async () => {
    renderComponent();

    const nameInput = screen.getByPlaceholderText("Placeholder1");
    userEvent.type(nameInput, ".");
    userEvent.clear(nameInput);
    await waitFor(() => {
      expect(screen.getByText(Texts.inputAppNameWarning)).toBeInTheDocument();
    });

    const uriInput = screen.getByPlaceholderText("Placeholder2");
    userEvent.type(uriInput, ".");
    userEvent.clear(uriInput);
    await waitFor(() => {
      expect(screen.getByText(Texts.inputAppURIWarning)).toBeInTheDocument();
    });
  });

  test("Click on submit button with wrong values", async () => {
    renderComponent();

    const nameInput = screen.getByPlaceholderText("Placeholder1");
    userEvent.type(nameInput, ".");
    userEvent.clear(nameInput);

    const uriInput = screen.getByPlaceholderText("Placeholder2");
    userEvent.type(uriInput, ".");

    userEvent.click(screen.getByRole("button"));
    await waitFor(() => {
      expect(screen.getByText(Texts.formError)).toBeInTheDocument();
    });
  });

  test("Click on submit button with correct values", async () => {
    renderComponent();

    userEvent.type(screen.getByPlaceholderText("Placeholder1"), "TestName");
    userEvent.type(screen.getByPlaceholderText("Placeholder2"), "TestUri");

    userEvent.click(screen.getByRole("button"));
    await waitFor(() => {
      expect(onValuesSetMock).toBeCalledWith({ name: "TestName", uri: "TestUri" });
    });
  });
});
