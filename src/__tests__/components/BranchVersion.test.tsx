import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import selectEvent from "react-select-event";
import BranchVersion from "../../components/BranchVersion";
import { BranchesDataResponse } from "../../responseTypes";

describe("BranchVersion Component Tests Suite", () => {
  const mockData: BranchesDataResponse = { branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: [] }] };
  const readyToComparePromise = Promise.resolve();
  const readyToCompareCallback = jest.fn(() => readyToComparePromise);
  const resetPromise = Promise.resolve();
  const resetCallback = jest.fn(() => resetPromise);
  const onBranchSelected = jest.fn();
  const onVersionSelected = jest.fn();
  const onDeleteBranch = jest.fn();
  const onDeleteVersion = jest.fn();

  function renderComponent(defaultValue: string = "", useDeleteButtons: boolean = false) {
    return render(
      <BranchVersion
        branchesData={mockData}
        defaultValue={defaultValue}
        readyToCompare={readyToCompareCallback}
        reset={resetCallback}
        useDeleteButtons={useDeleteButtons}
        onBranchSelected={onBranchSelected}
        onVersionSelected={onVersionSelected}
        onDeleteBranch={onDeleteBranch}
        onDeleteVersion={onDeleteVersion}
      />
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getAllByRole("combobox")).toHaveLength(2);
  });

  test("Renders component with default value", async () => {
    renderComponent("Test1...1.0.0");

    expect(screen.getAllByRole("combobox")).toHaveLength(2);
  });

  test("Renders elements correctly", async () => {
    renderComponent();

    const comboBoxes = screen.getAllByRole("combobox");

    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    expect(screen.getAllByText("Test1")).toHaveLength(2);
    expect(screen.getAllByText("Test2")).toHaveLength(2);

    act(() => {
      userEvent.click(comboBoxes[1]);
    });

    expect(screen.queryByText("1.0.0")).not.toBeInTheDocument();

    await act(() => readyToComparePromise);
  });

  test("Select branch with versions", async () => {
    renderComponent();

    const comboBoxes = screen.getAllByRole("combobox");
    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    expect(screen.getAllByRole("option")).toHaveLength(2);
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    act(() => {
      userEvent.click(comboBoxes[1]);
    });

    expect(screen.getAllByText("1.0.0")).toHaveLength(2);
    expect(screen.getAllByText("2.0.0")).toHaveLength(2);
    expect(screen.getAllByRole("option")).toHaveLength(4);

    await act(() => readyToComparePromise);
  });

  test("Select branch without versions", async () => {
    renderComponent();

    const comboBoxes = screen.getAllByRole("combobox");
    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    expect(screen.getAllByRole("option")).toHaveLength(2);
    await selectEvent.select(screen.getByRole("listbox"), ["Test2"]);

    act(() => {
      userEvent.click(comboBoxes[1]);
    });

    expect(screen.getAllByRole("option")).toHaveLength(2);

    await act(() => readyToComparePromise);
  });

  test("Select branch and versions", async () => {
    renderComponent();

    const comboBoxes = screen.getAllByRole("combobox");
    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    expect(onBranchSelected).toBeCalledWith("Test1");

    act(() => {
      userEvent.click(comboBoxes[1]);
    });
    await selectEvent.select(screen.getAllByRole("listbox")[1], ["1.0.0"]);

    expect(onVersionSelected).toBeCalledWith("1.0.0");

    expect(readyToCompareCallback).toBeCalled();
    await act(() => readyToComparePromise);
  });

  test("Delete functionality", async () => {
    renderComponent("", true);

    let buttons = screen.getAllByRole("button");
    let deleteBranchButton = buttons[0];
    let deleteVersionButton = buttons[1];
    expect(deleteBranchButton).toBeDisabled();
    expect(deleteVersionButton).toBeDisabled();

    const comboBoxes = screen.getAllByRole("combobox");
    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    buttons = screen.getAllByRole("button");
    deleteBranchButton = buttons[0];
    expect(deleteBranchButton).toBeEnabled();
    // Opening the confirmation popup
    userEvent.click(deleteBranchButton);
    expect(screen.getByRole("tooltip")).toBeInTheDocument();
    buttons = screen.getAllByRole("button");
    expect(buttons).toHaveLength(4);
    // Confirm delete
    userEvent.click(buttons[3]);
    expect(onDeleteBranch).toBeCalled();

    act(() => {
      userEvent.click(comboBoxes[1]);
    });
    await selectEvent.select(screen.getAllByRole("listbox")[1], ["1.0.0"]);

    buttons = screen.getAllByRole("button");
    deleteVersionButton = buttons[1];
    expect(deleteVersionButton).toBeEnabled();
    // Opening the confirmation popup
    userEvent.click(deleteVersionButton);
    expect(screen.getAllByRole("tooltip")[1]).toBeInTheDocument();
    buttons = screen.getAllByRole("button");
    expect(buttons).toHaveLength(6);
    // Confirm delete
    userEvent.click(buttons[5]);
    expect(onDeleteVersion).toBeCalled();
  });
});
