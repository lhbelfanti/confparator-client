import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter } from "react-router-dom";
import Comparator from "../../components/Comparator";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";

describe("Comparator Component Tests Suite", () => {
  const urlMock = "branch1:1.0.0...branch2:2.0.0";
  const data = {
    configs: [
      { "Config1.json": { old: { valueA: 1, valueB: "2" }, new: { valueA: 2, valueB: "2", valueC: "testing" } } },
      { "Config2.json": { old: { Text1: "Empty" }, new: {} } }
    ]
  };

  const server = setupServer(
    rest.get(`${API.compare}/${urlMock}`, (req, res, ctx) => {
      return res(ctx.json(data));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={[`/compare/${urlMock}`]}>
        <Comparator url={urlMock} />
      </MemoryRouter>
    );
  }

  test("Renders correctly", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getByText("Config1.json").closest("tr")).toBeInTheDocument();
      expect(screen.getByText(Texts.splitView)).toBeInTheDocument();
    });
  });

  test("On switch clicked", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getByText(Texts.splitView)).toBeInTheDocument();
    });

    act(() => {
      userEvent.click(screen.getByText(Texts.splitView));
    });

    expect(screen.getByText(Texts.normalView)).toBeInTheDocument();
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(`${API.compare}/${urlMock}`, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // The Retry button
    });

    server.use(
      rest.get(`${API.compare}/${urlMock}`, (req, res, ctx) => {
        return res(ctx.json(data));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getByText("Config1.json").closest("tr")).toBeInTheDocument();
      expect(screen.getByText(Texts.splitView)).toBeInTheDocument();
    });
  });
});
