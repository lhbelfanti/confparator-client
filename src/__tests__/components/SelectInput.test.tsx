import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import selectEvent from "react-select-event";
import SelectInput from "../../components/SelectInput";
import * as Texts from "../../configs/texts.json";
import { versionRegex } from "../../utils/regexParser";

describe("SelectInput Component Tests Suite", () => {
  const dataArrayMock = ["Element1", "Element2"];
  const onButtonClicked = jest.fn();

  function renderComponent(data: string[] = dataArrayMock, regex?: RegExp) {
    return render(
      <SelectInput
        data={data}
        selectTitle={Texts.existingBranches}
        selectPlaceholderText={Texts.selectBranch}
        inputTitle={Texts.createBranch}
        inputPlaceholderText={Texts.branch}
        buttonText={Texts.continue}
        onButtonClicked={onButtonClicked}
        regex={regex}
      />
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toBeInTheDocument();
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument();
    expect(button).toBeDisabled();
    expect(screen.getByText(Texts.existingBranches)).toBeInTheDocument();
    expect(screen.getByText(Texts.selectBranch)).toBeInTheDocument();
    expect(screen.getByText(Texts.createBranch)).toBeInTheDocument();
  });

  test("Renders correctly without data", () => {
    renderComponent([]);

    const combobox = screen.getByRole("combobox");
    expect(combobox).toBeInTheDocument();
    expect(combobox).toBeDisabled();
    expect(screen.getByRole("textbox")).toBeInTheDocument();
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument();
    expect(button).toBeDisabled();
    expect(screen.getByText(Texts.existingBranches)).toBeInTheDocument();
    expect(screen.getByText(Texts.selectBranch)).toBeInTheDocument();
    expect(screen.getByText(Texts.createBranch)).toBeInTheDocument();
  });

  test("Exclusive selection between the select and the input", async () => {
    renderComponent();

    expect(screen.queryByRole("option")).not.toBeInTheDocument();
    expect(screen.getByRole("textbox").getAttribute("value")).toBe("");

    userEvent.click(screen.getByRole("combobox"));
    await selectEvent.select(screen.getByRole("listbox"), ["Element1"]);
    let option = screen.getByRole("option", { name: "Element1" });
    expect(option.getAttribute("aria-selected")).toBe("true");
    expect(screen.getByRole("textbox").getAttribute("value")).toBe("");

    userEvent.type(screen.getByRole("textbox"), "Test");
    expect(screen.getByRole("textbox").getAttribute("value")).toBe("Test");
    option = screen.getByRole("option", { name: "Element1" });
    expect(option.getAttribute("aria-selected")).toBe("false");
  });

  test("Regex behaviour", () => {
    renderComponent([], versionRegex);

    const button = screen.getByRole("button");
    expect(button).toBeDisabled();
    expect(screen.queryByRole("tooltip")).not.toBeInTheDocument();

    const input = screen.getByRole("textbox");
    userEvent.type(input, "T");
    expect(button).toBeDisabled();
    expect(screen.getByRole("tooltip")).toBeInTheDocument();

    userEvent.type(input, "{backspace}");
    userEvent.type(input, "1");
    expect(button).toBeEnabled();
  });

  test("On button clicked", () => {
    renderComponent();

    const button = screen.getByRole("button");
    expect(button).toBeDisabled();

    userEvent.type(screen.getByRole("textbox"), "T");
    expect(button).toBeEnabled();

    userEvent.click(button);
    expect(onButtonClicked).toBeCalled();
  });
});
