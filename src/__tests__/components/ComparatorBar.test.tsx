import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import { MemoryRouter } from "react-router-dom";
import selectEvent from "react-select-event";
import ComparatorBar from "../../components/ComparatorBar";
import { BranchesDataResponse } from "../../responseTypes";

describe("ComparatorBar Component Tests Suite", () => {
  const mockData: BranchesDataResponse = {
    branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["3.0.0"] }]
  };
  const onCompareClickedPromise = Promise.resolve();
  const onCompareClicked = jest.fn(() => onCompareClickedPromise);

  function renderComponent(comparatorUrl: string = "") {
    const initialEntries = "/compare/testApp" + comparatorUrl ? `/${comparatorUrl}` : "";

    return render(
      <MemoryRouter initialEntries={[initialEntries]}>
        <ComparatorBar comparatorUrl={comparatorUrl} branchesData={mockData} onCompareClicked={onCompareClicked} />
      </MemoryRouter>
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getAllByRole("combobox")).toHaveLength(4);
    expect(screen.getByRole("button")).toBeDisabled();
  });

  test("Renders correctly with default value", () => {
    renderComponent("Test1:1.0.0...Test2:3.0.0");

    expect(screen.getAllByRole("combobox")).toHaveLength(4);
    expect(screen.getByRole("button")).toBeEnabled();
  });

  test("After selecting one option on each select the button should be enabled", async () => {
    renderComponent();

    const compareButton = screen.getByRole("button");
    expect(compareButton).toBeDisabled();

    const comboBoxes = screen.getAllByRole("combobox");

    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    act(() => {
      userEvent.click(comboBoxes[1]);
    });
    await selectEvent.select(screen.getAllByRole("listbox")[1], ["1.0.0"]);

    act(() => {
      userEvent.click(comboBoxes[2]);
    });
    await selectEvent.select(screen.getAllByRole("listbox")[2], ["Test1"]);

    act(() => {
      userEvent.click(comboBoxes[3]);
    });
    await selectEvent.select(screen.getAllByRole("listbox")[3], ["1.0.0"]);

    expect(compareButton).toBeEnabled();

    act(() => {
      userEvent.click(compareButton);
    });
    expect(onCompareClicked).toBeCalled();

    await act(() => onCompareClickedPromise);
  });
});
