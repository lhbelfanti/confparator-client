import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import selectEvent from "react-select-event";
import AppSelect from "../../components/AppSelect";

describe("AppSelect Component Tests Suite", () => {
  const mockArray = ["Element1", "Element2"];
  const placeholderText = "Placeholder";
  const onValueChangePromise = Promise.resolve();
  const onValueChangeMock = jest.fn(() => onValueChangePromise);
  const defaultValueMock = "Element1";

  function renderComponent(data: string[] | undefined = mockArray, defaultValue: string = "") {
    return render(
      <AppSelect
        placeholderText={placeholderText}
        data={data}
        onValueChange={onValueChangeMock}
        defaultValue={defaultValue}
        resetValue={false}
      />
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByText(placeholderText)).toBeInTheDocument();
  });

  test("Renders correctly with default value", () => {
    renderComponent(mockArray, defaultValueMock);

    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByText(defaultValueMock)).toBeInTheDocument();
  });

  test("Renders correctly without data but with a default value", () => {
    renderComponent([], defaultValueMock);

    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByText(defaultValueMock)).toBeInTheDocument();
  });

  test("Renders correctly with invalid", () => {
    const { rerender } = renderComponent(mockArray, defaultValueMock);

    rerender(
      <AppSelect
        placeholderText={placeholderText}
        data={[]}
        onValueChange={onValueChangeMock}
        defaultValue={defaultValueMock}
      />
    );

    expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByText(placeholderText)).toBeInTheDocument();
  });

  test("Renders elements correctly", async () => {
    renderComponent();

    userEvent.click(screen.getByRole("combobox"));

    expect(screen.getAllByText(mockArray[0])).toHaveLength(2);
    expect(screen.getAllByText(mockArray[1])).toHaveLength(2);

    await act(() => onValueChangePromise);
  });

  test("Select an element", async () => {
    renderComponent();

    const comboBox = screen.getByRole("combobox");
    userEvent.click(comboBox);
    await selectEvent.select(comboBox, [mockArray[0]]);
    expect(onValueChangeMock).toBeCalledWith(mockArray[0]);

    await act(() => onValueChangePromise);
  });

  test("Reset values", async () => {
    const { rerender } = renderComponent();

    const comboBox = screen.getByRole("combobox");
    userEvent.click(comboBox);
    await selectEvent.select(comboBox, [mockArray[0]]);
    expect(onValueChangeMock).toBeCalledWith(mockArray[0]);
    let option = screen.getByRole("option", { name: "Element1" });
    expect(option.getAttribute("aria-selected")).toBe("true");
    await act(() => onValueChangePromise);

    rerender(
      <AppSelect
        placeholderText={placeholderText}
        data={mockArray}
        onValueChange={onValueChangeMock}
        resetValue={true}
      />
    );

    option = screen.getByRole("option", { name: "Element1" });
    expect(option.getAttribute("aria-selected")).toBe("false");
  });
});
