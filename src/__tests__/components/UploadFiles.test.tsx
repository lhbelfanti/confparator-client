import { act, fireEvent, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import UploadFiles from "../../components/UploadFiles";
import * as Texts from "../../configs/texts.json";

// These test were incredibly difficult to set up (they took me 6 hours). They are working now.
describe("UploadFiles Component Tests Suite", () => {
  const server = setupServer(
    rest.post("/test", (req, res, ctx) => {
      return res(ctx.json({ response: "200 OK" }));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  const onButtonClicked = jest.fn();

  function renderComponent() {
    return render(<UploadFiles uploadToUrl={"test"} buttonText={Texts.finish} onButtonClicked={onButtonClicked} />);
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getAllByRole("button")).toHaveLength(2);
    expect(screen.getByText(Texts.finish)).toBeInTheDocument();
  });

  test("Upload files and click button", async () => {
    const flushPromises = () => new Promise(setImmediate);
    renderComponent();

    const button = screen.getAllByRole("button")[1];
    const file = new File(["{test: 1}"], "test.json", { type: "application/json" });
    const input: HTMLInputElement = screen.getByTestId("uploader") as HTMLInputElement;
    expect(input.files).toHaveLength(0);
    expect(button).toBeDisabled();

    await act(async () => {
      fireEvent.change(input, { target: { files: [file] } });
    });
    await flushPromises();

    await waitFor(() => {
      expect(screen.getByText(`test.json ${Texts.messageUploadSuccess}`)).toBeInTheDocument();
    });

    expect(input.files).toHaveLength(1);
    expect(button).toBeEnabled();
    act(() => {
      userEvent.click(button);
    });
    expect(onButtonClicked).toBeCalled();
  });

  test("Upload fail", async () => {
    server.use(
      rest.post("/test", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    const flushPromises = () => new Promise(setImmediate);
    renderComponent();

    await act(async () => {
      const button = screen.getAllByRole("button")[1];
      const file = new File(["{test: 1}"], "test.json", { type: "application/json" });
      const input: HTMLInputElement = screen.getByTestId("uploader") as HTMLInputElement;
      expect(input.files).toHaveLength(0);
      expect(button).toBeDisabled();
      fireEvent.change(input, { target: { files: [file] } });
      expect(input.files).toHaveLength(1);
      expect(button).toBeDisabled();
      await flushPromises();
    });

    await waitFor(() => {
      expect(screen.getByText(`test.json ${Texts.messageUploadFail}`)).toBeInTheDocument();
    });
  });
});
