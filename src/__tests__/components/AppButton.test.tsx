import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Location } from "history";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import AppButton from "../../components/AppButton";

describe("AppButton Component Tests Suite", () => {
  const buttonTextMock = "Test";
  const onConfirmClicked = jest.fn();
  const onCancelClicked = jest.fn();
  const onClickCallback = jest.fn();

  afterEach(cleanup);

  let currentPath: Location<unknown>;
  function renderComponent(disabled: boolean, path: string | undefined, confirmationPopup: boolean = false) {
    return render(
      <MemoryRouter initialEntries={["/"]}>
        <Route
          path="*"
          render={(props) => {
            currentPath = props.location;

            return (
              <AppButton
                text={buttonTextMock}
                size={"small"}
                disabled={disabled}
                path={path}
                onClick={onClickCallback}
                confirmationPopup={confirmationPopup}
                popupTitle={"Popup Title"}
                onConfirm={onConfirmClicked}
                onCancel={onCancelClicked}
                style={{ marginTop: "15px" }}
              />
            );
          }}
        />
      </MemoryRouter>
    );
  }

  test("Button text", () => {
    renderComponent(true, undefined);

    expect(screen.getByText(buttonTextMock)).toBeInTheDocument();
  });

  test("Disabled state", () => {
    renderComponent(true, undefined);

    expect(screen.getByRole("button")).toBeDisabled();
  });

  test("Click callback", () => {
    renderComponent(false, undefined);

    userEvent.click(screen.getByRole("button"));
    expect(onClickCallback).toBeCalledTimes(1);
  });

  test("Link functionality", () => {
    const pathMock = "test";
    renderComponent(false, pathMock);

    expect(currentPath.pathname).toBe("/");

    userEvent.click(screen.getByRole("button"));
    expect(onClickCallback).toBeCalledTimes(1);

    expect(currentPath.pathname).toBe(`/${pathMock}`);
  });

  test("Confirmation popup functionality", () => {
    renderComponent(false, undefined, true);

    userEvent.click(screen.getByRole("button"));
    expect(screen.getByRole("tooltip")).toBeInTheDocument();
    const buttons = screen.getAllByRole("button");
    expect(buttons).toHaveLength(3);

    // Confirm button
    userEvent.click(buttons[2]);
    expect(onClickCallback).not.toBeCalled();
    expect(onConfirmClicked).toBeCalled();
    expect(onCancelClicked).not.toBeCalled();

    // Cancel button
    userEvent.click(buttons[1]);
    expect(onClickCallback).not.toBeCalled();
    expect(onCancelClicked).toBeCalled();
  });
});
