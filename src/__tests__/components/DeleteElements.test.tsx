import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import selectEvent from "react-select-event";
import DeleteElements from "../../components/DeleteElements";
import * as API from "../../configs/api.json";
import * as Texts from "../../configs/texts.json";
import { BranchesDataResponse } from "../../responseTypes";

describe("DeleteElements Component Tests Suite", () => {
  const dataMock: BranchesDataResponse = {
    branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["3.0.0"] }]
  };

  const server = setupServer(
    rest.delete(API.delete, (req, res, ctx) => {
      return res(ctx.json({ response: "200 OK" }));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={["/manage/test/delete"]}>
        <Route
          path={"/manage/:app/delete"}
          render={() => {
            return <DeleteElements branchesData={dataMock} app={"test"} />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getByText(Texts.deleteSection)).toBeInTheDocument();
    expect(screen.getAllByRole("combobox")).toHaveLength(2);
    expect(screen.getAllByRole("button")).toHaveLength(3);
    expect(screen.getByText(Texts.dangerArea)).toBeInTheDocument();
    expect(screen.getByText(Texts.deleteMessage)).toBeInTheDocument();
  });

  test("On branch delete", async () => {
    renderComponent();

    let buttons = screen.getAllByRole("button");
    expect(buttons[0]).toBeDisabled();
    act(() => {
      userEvent.click(screen.getAllByRole("combobox")[0]);
    });
    expect(screen.getAllByRole("option")).toHaveLength(2);
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    buttons = screen.getAllByRole("button");
    expect(buttons[0]).toBeEnabled();
    // Opening the confirmation popup
    userEvent.click(buttons[0]);
    expect(screen.getByRole("tooltip")).toBeInTheDocument();

    await waitFor(() => {
      buttons = screen.getAllByRole("button");
      expect(buttons).toHaveLength(5);
    });

    // Confirm delete
    userEvent.click(buttons[4]);

    await waitFor(() => {
      buttons = screen.getAllByRole("button");
      expect(buttons).toHaveLength(1); // The continue button of the DELETE Request success screen
    });
  });

  test("On version delete", async () => {
    renderComponent();

    let buttons = screen.getAllByRole("button");
    expect(buttons[1]).toBeDisabled();
    const comboBoxes = screen.getAllByRole("combobox");
    act(() => {
      userEvent.click(comboBoxes[0]);
    });
    expect(screen.getAllByRole("option")).toHaveLength(2);
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    act(() => {
      userEvent.click(comboBoxes[1]);
    });
    await selectEvent.select(screen.getAllByRole("listbox")[1], ["1.0.0"]);

    buttons = screen.getAllByRole("button");
    expect(buttons[1]).toBeEnabled();
    // Opening the confirmation popup
    userEvent.click(buttons[1]);
    expect(screen.getByRole("tooltip")).toBeInTheDocument();

    await waitFor(() => {
      buttons = screen.getAllByRole("button");
      expect(buttons).toHaveLength(5);
    });

    // Confirm delete
    userEvent.click(buttons[4]);

    await waitFor(() => {
      buttons = screen.getAllByRole("button");
      expect(buttons).toHaveLength(1); // The continue button of the DELETE Request success screen
    });
  });

  test("On app delete", async () => {
    renderComponent();

    let buttons = screen.getAllByRole("button");
    expect(buttons[2]).toBeEnabled();

    // Opening the confirmation popup
    userEvent.click(buttons[2]);
    expect(screen.getByRole("tooltip")).toBeInTheDocument();

    await waitFor(() => {
      buttons = screen.getAllByRole("button");
      expect(buttons).toHaveLength(5);
    });

    // Confirm delete
    userEvent.click(buttons[4]);

    await waitFor(() => {
      buttons = screen.getAllByRole("button");
      expect(buttons).toHaveLength(1); // The continue button of the DELETE Request success screen
    });
  });
});
