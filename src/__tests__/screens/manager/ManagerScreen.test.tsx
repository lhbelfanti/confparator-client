import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import ManagerScreen from "../../../screens/manager/ManagerScreen";

describe("ManagerScreen Tests Suite", () => {
  const app = "Test";
  const responseMock = { name: "Test" };

  const server = setupServer(
    rest.get(API.validate, (req, res, ctx) => {
      return res(ctx.json(responseMock));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  let currentPath: Location<unknown>;
  function renderComponent() {
    const url = `/manage/${app}/`;
    const path = "/manage/:app";

    return render(
      <MemoryRouter initialEntries={[url]}>
        <Route
          path={path}
          render={(props) => {
            currentPath = props.location;

            return <ManagerScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getAllByRole("button")).toHaveLength(3);
      expect(screen.getByText(Texts.selectedApplication)).toBeInTheDocument();
      expect(screen.getByText(Texts.manage)).toBeInTheDocument();
      expect(screen.getByText(Texts.renameApp)).toBeInTheDocument();
      expect(screen.getByText(Texts.uploadConfigs)).toBeInTheDocument();
      expect(screen.getByText(Texts.deleteSection)).toBeInTheDocument();
      expect(screen.getByText(app)).toBeInTheDocument();
    });
  });

  test("On Rename App clicked", async () => {
    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.renameApp);
      expect(button).toBeInTheDocument();
    });

    act(() => {
      userEvent.click(button);
    });

    expect(currentPath.pathname).toBe(`/manage/${app}/rename/`);
  });

  test("On Upload config files clicked", async () => {
    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.uploadConfigs);
      expect(button).toBeInTheDocument();
    });

    act(() => {
      userEvent.click(button);
    });

    expect(currentPath.pathname).toBe(`/manage/${app}/upload/`);
  });

  test("On Delete Section clicked", async () => {
    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.deleteSection);
      expect(button).toBeInTheDocument();
    });

    act(() => {
      userEvent.click(button);
    });

    expect(currentPath.pathname).toBe(`/manage/${app}/delete/`);
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(API.validate, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.retry);
      expect(button).toBeInTheDocument();
    });

    server.use(
      rest.get(API.validate, (req, res, ctx) => {
        return res(ctx.json(responseMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getAllByRole("button")).toHaveLength(3);
      expect(screen.getByText(Texts.selectedApplication)).toBeInTheDocument();
      expect(screen.getByText(Texts.manage)).toBeInTheDocument();
      expect(screen.getByText(Texts.renameApp)).toBeInTheDocument();
      expect(screen.getByText(Texts.uploadConfigs)).toBeInTheDocument();
      expect(screen.getByText(Texts.deleteSection)).toBeInTheDocument();
      expect(screen.getByText(app)).toBeInTheDocument();
    });
  });
});
