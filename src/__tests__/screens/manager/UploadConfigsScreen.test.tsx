import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import * as API from "../../../configs/api.json";
import UploadConfigsScreen from "../../../screens/manager/UploadConfigsScreen";

describe("UploadConfigsScreen Tests Suite", () => {
  const app = "Test";
  const responseMock = {
    branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["2.0.0"] }]
  };

  const server = setupServer(
    rest.get(`${API.list}/${app}`, (req, res, ctx) => {
      return res(ctx.json(responseMock));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={[`/manage/${app}/upload`]}>
        <Route
          path={"/manage/:app/upload"}
          render={() => {
            return <UploadConfigsScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly.", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getAllByRole("button")).toHaveLength(4);
    });
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(`${API.list}/${app}`, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // The Retry button
    });

    server.use(
      rest.get(`${API.list}/${app}`, (req, res, ctx) => {
        return res(ctx.json(responseMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getAllByRole("button")).toHaveLength(4);
    });
  });
});
