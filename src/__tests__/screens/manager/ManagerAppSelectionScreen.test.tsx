import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import selectEvent from "react-select-event";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import ManagerAppSelectionScreen from "../../../screens/manager/ManagerAppSelectionScreen";

describe("ManagerAppSelectionScreen Tests Suite", () => {
  const jsonDataMock = {
    apps: [
      { uri: "app1", name: "Test1" },
      { uri: "app2", name: "Test2" },
      { uri: "app3", name: "Test3" }
    ]
  };

  const server = setupServer(
    rest.get(API.list, (req, res, ctx) => {
      return res(ctx.json(jsonDataMock));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  let currentPath: Location<unknown>;
  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={["/manage/"]}>
        <Route
          path="*"
          render={(props) => {
            currentPath = props.location;

            return <ManagerAppSelectionScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly. On fetch success with data", async () => {
    renderComponent();

    await waitFor(() => {
      const buttons = screen.getAllByRole("button");
      expect(buttons[0]).toBeDisabled();
      expect(buttons[1]).toBeEnabled();
      expect(screen.getByText(Texts.selectApp)).toBeInTheDocument();
      expect(screen.getByText(Texts.createNewApp)).toBeInTheDocument();
    });
  });

  test("On app selected after the fetch success", async () => {
    renderComponent();

    await waitFor(() => {
      const buttons = screen.getAllByRole("button");
      expect(buttons[0]).toBeDisabled();
      expect(buttons[1]).toBeEnabled();
    });

    const comboBox = screen.getByRole("combobox");
    act(() => {
      userEvent.click(comboBox);
    });
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    const buttonsInScreen = screen.getAllByRole("button");
    expect(buttonsInScreen[0]).toBeEnabled();

    act(() => {
      userEvent.click(buttonsInScreen[0]);
    });

    expect(currentPath.pathname).toBe(`/manage/${jsonDataMock.apps[0].uri}/`);
  });

  test("On Create new App clicked", async () => {
    renderComponent();

    let createNewAppBtn: TargetElement;
    await waitFor(() => {
      createNewAppBtn = screen.getByText(Texts.createNewApp);
      expect(createNewAppBtn).toBeInTheDocument();
    });

    act(() => {
      userEvent.click(createNewAppBtn);
    });

    expect(currentPath.pathname).toBe("/manage/create/");
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(API.list, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.retry);
      expect(button).toBeInTheDocument();
      expect(screen.getByText(Texts.createNewApp)).toBeInTheDocument();
    });

    server.use(
      rest.get(API.list, (req, res, ctx) => {
        return res(ctx.json(jsonDataMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getAllByRole("button")[0]).toBeDisabled();
      expect(screen.getByText(Texts.selectApp)).toBeInTheDocument();
      expect(screen.getByText(Texts.createNewApp)).toBeInTheDocument();
    });
  });
});
