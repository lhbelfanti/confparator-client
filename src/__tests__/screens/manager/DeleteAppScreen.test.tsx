import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import DeleteAppScreen from "../../../screens/manager/DeleteAppScreen";

describe("DeleteAppScreen Tests Suite", () => {
  const app = "Test";
  const responseMock = {
    branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["2.0.0"] }]
  };

  const server = setupServer(
    rest.get(`${API.list}/${app}`, (req, res, ctx) => {
      return res(ctx.json(responseMock));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={[`/manage/${app}/delete`]}>
        <Route
          path={"/manage/:app/delete"}
          render={() => {
            return <DeleteAppScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly.", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getByText(Texts.deleteSection)).toBeInTheDocument();
      expect(screen.getAllByRole("combobox")).toHaveLength(2);
      expect(screen.getAllByRole("button")).toHaveLength(3);
      expect(screen.getByText(Texts.dangerArea)).toBeInTheDocument();
      expect(screen.getByText(Texts.deleteMessage)).toBeInTheDocument();
    });
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(`${API.list}/${app}`, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.retry);
      expect(button).toBeInTheDocument();
    });

    server.use(
      rest.get(`${API.list}/${app}`, (req, res, ctx) => {
        return res(ctx.json(responseMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getByText(Texts.deleteSection)).toBeInTheDocument();
      expect(screen.getAllByRole("combobox")).toHaveLength(2);
      expect(screen.getAllByRole("button")).toHaveLength(3);
      expect(screen.getByText(Texts.dangerArea)).toBeInTheDocument();
      expect(screen.getByText(Texts.deleteMessage)).toBeInTheDocument();
    });
  });
});
