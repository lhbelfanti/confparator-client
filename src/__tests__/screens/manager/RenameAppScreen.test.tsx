import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import RenameAppScreen from "../../../screens/manager/RenameAppScreen";

describe("RenameAppScreen Tests Suite", () => {
  const app = "app1";

  const jsonDataMock = {
    apps: [
      { uri: "app1", name: "Test1" },
      { uri: "app2", name: "Test2" },
      { uri: "app3", name: "Test3" }
    ]
  };

  const server = setupServer(
    rest.get(`${API.list}`, (req, res, ctx) => {
      return res(ctx.json(jsonDataMock));
    }),
    rest.put(`${API.rename}`, (req, res, ctx) => {
      return res(ctx.json({ status: 200 }));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  let currentPath: Location<unknown>;
  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={[`/manage/${app}/rename`]}>
        <Route
          path={["/manage/:app/rename", "/manage/"]}
          render={(props) => {
            currentPath = props.location;

            return <RenameAppScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getAllByRole("textbox")).toHaveLength(2);
      expect(screen.getByRole("button")).toBeInTheDocument();
    });
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(`${API.list}`, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // The Retry button
    });

    server.use(
      rest.get(`${API.list}`, (req, res, ctx) => {
        return res(ctx.json(jsonDataMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getAllByRole("textbox")).toHaveLength(2);
      expect(screen.getByRole("button")).toBeInTheDocument();
    });
  });

  test("PUT request", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getAllByRole("textbox")).toHaveLength(2);
      expect(screen.getByRole("button")).toBeInTheDocument();
    });

    userEvent.type(screen.getByPlaceholderText(Texts.namePlaceholder), "TestName");
    userEvent.type(screen.getByPlaceholderText(Texts.uriPlaceholder), "TestUri");

    userEvent.click(screen.getByRole("button"));

    // Rename success
    await waitFor(() => {
      expect(screen.getByText(Texts.renamedSuccessfully)).toBeInTheDocument();
    });

    userEvent.click(screen.getByText(Texts.continue));
    expect(currentPath.pathname).toBe("/manage/");
  });
});
