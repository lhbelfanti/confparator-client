import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import CreateNewAppScreen from "../../../screens/manager/CreateNewAppScreen";

describe("CreateNewAppScreen Tests Suite", () => {
  const server = setupServer(
    rest.post(`${API.create}`, (req, res, ctx) => {
      return res(ctx.json({ status: 200 }));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  let currentPath: Location<unknown>;
  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={["/manage/create"]}>
        <Route
          path={["/manage/create", "/manage/"]}
          render={(props) => {
            currentPath = props.location;

            return <CreateNewAppScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly", async () => {
    renderComponent();

    expect(screen.getAllByRole("textbox")).toHaveLength(2);
    expect(screen.getByRole("button")).toBeInTheDocument();
  });

  test("POST request", async () => {
    renderComponent();

    userEvent.type(screen.getByPlaceholderText(Texts.namePlaceholder), "TestName");
    userEvent.type(screen.getByPlaceholderText(Texts.uriPlaceholder), "TestUri");

    userEvent.click(screen.getByRole("button"));

    // Rename success
    await waitFor(() => {
      expect(screen.getByText(Texts.createdSuccessfully)).toBeInTheDocument();
    });

    userEvent.click(screen.getByText(Texts.continue));
    expect(currentPath.pathname).toBe("/manage/");
  });
});
