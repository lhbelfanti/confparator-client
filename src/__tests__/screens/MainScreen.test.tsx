import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Location } from "history";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import * as Texts from "../../configs/texts.json";
import MainScreen from "../../screens/MainScreen";

describe("MainScreen Tests Suite", () => {
  let currentPath: Location<unknown>;
  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={["/"]}>
        <Route
          path="*"
          render={(props) => {
            currentPath = props.location;

            return <MainScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly", () => {
    renderComponent();

    expect(screen.getAllByRole("button")).toHaveLength(2);
    expect(screen.getAllByRole("link")).toHaveLength(2);
  });

  test("Click compare button", () => {
    renderComponent();

    const compareButton = screen.getByText(Texts.compare);
    userEvent.click(compareButton);
    expect(currentPath.pathname).toBe("/compare/");
  });

  test("Click manage button", () => {
    renderComponent();

    const manageButton = screen.getByText(Texts.manage);
    userEvent.click(manageButton);
    expect(currentPath.pathname).toBe("/manage/");
  });
});
