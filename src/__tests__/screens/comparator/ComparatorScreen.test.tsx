import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import selectEvent from "react-select-event";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import ComparatorScreen from "../../../screens/comparator/ComparatorScreen";

describe("ComparatorScreen Tests Suite", () => {
  const responseMock = {
    branches: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["2.0.0"] }]
  };
  const compareResponseMock = {
    configs: [{ Test1: ["1.0.0", "2.0.0"] }, { Test2: ["2.0.0"] }]
  };
  const server = setupServer(
    rest.get(`${API.list}/testApp`, (req, res, ctx) => {
      return res(ctx.json(responseMock));
    }),
    rest.get(`${API.compare}/Test1:1.0.0...Test2:2.0.0`, (req, res, ctx) => {
      return res(ctx.json(compareResponseMock));
    }),
    rest.get(`${API.compare}/Test1:2.0.0...Test2:2.0.0`, (req, res, ctx) => {
      return res(ctx.json(compareResponseMock));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  function renderComponent(comparison: string = "") {
    const comparisonData = !!comparison ? `/${comparison}` : "";
    const url = "/compare/testApp" + comparisonData;
    const path = "/compare/:app" + (!!comparison ? "/:comparison" : "");

    return render(
      <MemoryRouter initialEntries={[url]}>
        <Route path={path}>
          <ComparatorScreen />
        </Route>
      </MemoryRouter>
    );
  }

  test("Renders correctly. On fetch success. Without comparison URL", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getByRole("button")).toBeInTheDocument();
      expect(screen.getAllByRole("combobox")).toHaveLength(4);

      // The selects should not have a value selected.
      expect(screen.queryByText("Test1")).not.toBeInTheDocument();
      expect(screen.queryByText("1.0.0")).not.toBeInTheDocument();
      expect(screen.queryByText("Test2")).not.toBeInTheDocument();
      expect(screen.queryByText("2.0.0")).not.toBeInTheDocument();
    });
  });

  test("Renders correctly. On fetch success. With comparison URL", async () => {
    renderComponent("Test1:1.0.0...Test2:2.0.0");

    await waitFor(() => {
      expect(screen.getByRole("button")).toBeInTheDocument();
      expect(screen.getAllByRole("combobox")).toHaveLength(4);

      expect(screen.getByText("Test1")).toBeInTheDocument();
      expect(screen.getByText("1.0.0")).toBeInTheDocument();
      expect(screen.getByText("Test2")).toBeInTheDocument();
      expect(screen.getByText("2.0.0")).toBeInTheDocument();
    });
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(`${API.list}/testApp`, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // The Retry button
    });

    server.use(
      rest.get(`${API.list}/testApp`, (req, res, ctx) => {
        return res(ctx.json(responseMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getAllByRole("button")).toHaveLength(1);
      expect(screen.getAllByRole("combobox")).toHaveLength(4);
    });
  });

  test("Clicks on compare button", async () => {
    renderComponent("Test1:1.0.0...Test2:2.0.0");

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByText(Texts.compare);
      expect(button).toBeInTheDocument(); // The compare button
    });

    act(() => {
      userEvent.click(screen.getAllByRole("combobox")[1]);
    });
    await selectEvent.select(screen.getByRole("listbox"), ["2.0.0"]);

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      // The fail image, because that call is not handled in this test (not needed).
      expect(screen.getByRole("img")).toBeInTheDocument();
    });
  });
});
