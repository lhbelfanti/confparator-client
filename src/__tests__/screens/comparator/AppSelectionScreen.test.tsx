import { act, render, screen, waitFor } from "@testing-library/react";
import userEvent, { TargetElement } from "@testing-library/user-event";
import { Location } from "history";
import { rest } from "msw";
import { setupServer } from "msw/node";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";
import selectEvent from "react-select-event";
import * as API from "../../../configs/api.json";
import * as Texts from "../../../configs/texts.json";
import AppSelectionScreen from "../../../screens/comparator/AppSelectionScreen";

describe("AppSelectionScreen Tests Suite", () => {
  const jsonDataMock = {
    apps: [
      { uri: "app1", name: "Test1" },
      { uri: "app2", name: "Test2" },
      { uri: "app3", name: "Test3" }
    ]
  };

  const server = setupServer(
    rest.get(API.list, (req, res, ctx) => {
      return res(ctx.json(jsonDataMock));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  let currentPath: Location<unknown>;
  function renderComponent() {
    return render(
      <MemoryRouter initialEntries={["/compare/"]}>
        <Route
          path="*"
          render={(props) => {
            currentPath = props.location;

            return <AppSelectionScreen />;
          }}
        />
      </MemoryRouter>
    );
  }

  test("Renders correctly. On fetch success with data", async () => {
    renderComponent();

    await waitFor(() => {
      expect(screen.getByRole("button")).toBeDisabled();
      expect(screen.getByText(Texts.selectApp)).toBeInTheDocument();
    });
  });

  test("On app selected after the fetch success", async () => {
    server.use(
      rest.get(API.list, (req, res, ctx) => {
        return res(ctx.json(jsonDataMock));
      })
    );

    renderComponent();

    await waitFor(() => {
      expect(screen.getByRole("button")).toBeDisabled();
    });

    const comboBox = screen.getByRole("combobox");
    act(() => {
      userEvent.click(comboBox);
    });
    await selectEvent.select(screen.getByRole("listbox"), ["Test1"]);

    const button = screen.getByRole("button");
    expect(button).toBeEnabled();

    act(() => {
      userEvent.click(button);
    });
    expect(currentPath.pathname).toBe(`/compare/${jsonDataMock.apps[0].uri}/`);
  });

  test("Renders correctly. On fetch success without data", async () => {
    server.use(
      rest.get(API.list, (req, res, ctx) => {
        return res(ctx.json({ apps: [] }));
      })
    );

    renderComponent();

    await waitFor(() => {
      expect(screen.queryByRole("button")).not.toBeInTheDocument();
    });
  });

  test("Renders correctly. On fetch fail", async () => {
    server.use(
      rest.get(API.list, (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    renderComponent();

    let button: TargetElement;
    await waitFor(() => {
      button = screen.getByRole("button");
      expect(button).toBeInTheDocument(); // The Retry button
    });

    server.use(
      rest.get(API.list, (req, res, ctx) => {
        return res(ctx.json(jsonDataMock));
      })
    );

    // Testing click on the retry button
    act(() => {
      userEvent.click(button);
    });

    await waitFor(() => {
      expect(screen.getByRole("button")).toBeDisabled();
      expect(screen.getByText(Texts.selectApp)).toBeInTheDocument();
    });
  });
});
