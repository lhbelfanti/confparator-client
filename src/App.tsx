import "antd/dist/antd.less";
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AppTitle from "./components/AppTitle";
import AppSelectionScreen from "./screens/comparator/AppSelectionScreen";
import ComparatorScreen from "./screens/comparator/ComparatorScreen";
import MainScreen from "./screens/MainScreen";
import CreateNewAppScreen from "./screens/manager/CreateNewAppScreen";
import DeleteAppScreen from "./screens/manager/DeleteAppScreen";
import ManagerAppSelectionScreen from "./screens/manager/ManagerAppSelectionScreen";
import ManagerScreen from "./screens/manager/ManagerScreen";
import RenameAppScreen from "./screens/manager/RenameAppScreen";
import UploadConfigsScreen from "./screens/manager/UploadConfigsScreen";

const App = () => {
  return (
    <div>
      <AppTitle />
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={MainScreen} />
          <Route exact path="/compare/" component={AppSelectionScreen} />
          <Route exact path="/compare/:app/" component={ComparatorScreen} />
          <Route exact path="/compare/:app/:comparison?" component={ComparatorScreen} />
          <Route exact path="/manage/" component={ManagerAppSelectionScreen} />
          <Route exact path="/manage/create/" component={CreateNewAppScreen} />
          <Route exact path="/manage/:app/" component={ManagerScreen} />
          <Route exact path="/manage/:app/rename/" component={RenameAppScreen} />
          <Route exact path="/manage/:app/delete/" component={DeleteAppScreen} />
          <Route exact path="/manage/:app/upload/" component={UploadConfigsScreen} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
